# PyCheby
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

'''
An example of the PyCheby parser for the RF_SPS200_LOOPS memory map.

To visualize the memory map, use the Reksio memory map viewer:
https://gitlab.cern.ch/Cheburashka/gui
'''
import logging

from PyCheby.PyCheby import PyCheby

def main():
    logging.basicConfig(encoding='utf-8', format='%(message)s', level=logging.INFO)
    logger = logging.getLogger(__name__)

    # Parse memory map
    tree = PyCheby('tests/maps/RF_SPS200_LOOPS/sps200CavityControl.cheby')
    topmap = tree.get()

    logger.info("################################################################################")
    logger.info(f'Top map "{topmap.name}" children')
    logger.info("################################################################################")
    for name, submap in topmap.children.items():
        logger.info(f'  {name:<9} = {submap}')

    logger.info('')
    logger.info("################################################################################")
    logger.info(f'"hwInfo" submap attributes and children')
    logger.info("################################################################################")
    hwinfo = topmap.children['hwInfo']

    logger.info('Submap attributes:')
    logger.info(f'type(hwInfo)      = "{type(hwinfo)}"')
    logger.info(f'hwInfo.name       = "{hwinfo.name}"')
    logger.info(f'hwInfo.bus        = "{hwinfo.bus}"')
    logger.info(f'hwInfo.address    = "{hex(hwinfo.address)}"')
    logger.info('')

    logger.info('HDL attribute container (stored as an Extension):')
    logger.info(f'type(hwInfo.hdl)              = "{type(hwinfo.hdl)}"')
    logger.info(f'hwInfo.hdl.name_suffix        = "{hwinfo.hdl.name_suffix}"')
    logger.info(f'hwInfo.hdl.bus_attribute      = "{hwinfo.hdl.bus_attribute}"')
    logger.info(f'hwInfo.hdl.bus_granularity    = "{hwinfo.hdl.bus_granularity}"')
    logger.info('')

    logger.info('Submap enum node (stored as a list of Extensions):')
    echo_enum = hwinfo.enumerations[0]
    logger.info(f'type(hwInfo.enumerations[0]) = "{type(echo_enum)}"')
    logger.info('')

    logger.info('Enum "echo":')
    for item_name, value in echo_enum.children.items():
        logger.info(f'Enumerator {item_name:<15} = {value}')


if __name__ == '__main__':
    main()
