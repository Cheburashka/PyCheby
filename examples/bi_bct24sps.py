# PyCheby
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

'''
An example of the PyCheby parser for the BI_BCT24SPS memory map.

To visualize the memory map, use the Reksio memory map viewer:
https://gitlab.cern.ch/Cheburashka/gui
'''
import logging

from PyCheby.PyCheby import PyCheby

def main():
    logging.basicConfig(encoding='utf-8', format='%(message)s', level=logging.INFO)
    logger = logging.getLogger(__name__)

    # Parse memory map
    tree = PyCheby('tests/maps/BCTDC-DIDT-SPS/BI_BCT24SPS.cheby')
    topmap = tree.get()

    logger.info("################################################################################")
    logger.info(f'Top map "{topmap.name}" and its children')
    logger.info("################################################################################")

    registers = topmap.children['Registers']
    logger.info(f'Address-Space "{registers.name}" = {registers}')

    logger.info(f'Address-Space "{registers.name}" children:')
    for name, child in registers.children.items():
        logger.info(f'  Child "{name}" = {child}')

    logger.info('')

    system = registers.children['System']
    logger.info(f'Submap "{system.name}" and its Repeat children:')
    for child in system.repeats:
        logger.info(f'  Repeat "{child.name}": address={hex(child.address):<5} count={child.count}')

    logger.info('')

    logger.info("################################################################################")
    logger.info('Special-purpose INTC (Interrupt-Controller) Registers')
    logger.info("################################################################################")

    # expected precisely only one INTC register
    reg, *_ = [node for node in topmap.walk() if node.is_register() and node.has_interrupt_controllers()]
    logger.info(f'INTC Register "{reg.name}": address={hex(reg.address)}, access="{reg.access}"')
    logger.info(f'INTC Register "{reg.name}": parent="{reg.parent.name}"')
    logger.info(f'INTC Register "{reg.name}": full_path="{reg.get_full_path()}"')

    # Expected precisely only one Interrupt Controller in the register
    intc, *_ = [child for child in reg.interrupt_controllers]
    logger.info(f'INTC Register Role "{intc.name}": value="{intc.type.value}"')


if __name__ == '__main__':
    main()
