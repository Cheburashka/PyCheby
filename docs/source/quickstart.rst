.. _quickstart:

===========
 Quickstart
===========

Installation
------------

To install **PyCheby**, open an interactive shell and run:

.. code::

    python3 -m pip install git+https://gitlab.cern.ch/Cheburashka/PyCheby.git#PyCheby==<VERSION>

or checkout **PyCheby** repository and install from a local folder:

.. code::

    git clone --branch <VERSION> https://gitlab.cern.ch/Cheburashka/PyCheby.git
    python3 -m pip install PyCheby/

Using PyCheby
-------------

To start using **PyCheby**, write a python script:

.. code::

    from PyCheby.PyCheby import PyCheby

    # Parse cheby map
    tree = PyCheby('tests/maps/RF_SPS200_LOOPS/sps200CavityControl.cheby')
    topmap = tree.get()

    # Topmap attributes
    print(f'Topmap name: "{topmap.name}"')
    print(f'Topmap bus: "{topmap.bus}"')
    print('')

    submaps = topmap.submaps
    for sb in submaps:
        print(f'Submap "{sb.name}": address={hex(sb.address)}')


Expected output:

.. code::

    Topmap name: "sps200CavityControl"
    Topmap bus: "axi4-lite-32"

    Submap "hwInfo": address=0x0
    Submap "sis8300ku": address=0x80000
    Submap "app": address=0x100000
    Submap "fgc_ddr": address=0x0
    Submap "acq_ddr": address=0x20000000
    Submap "acq_ram": address=0x80000000
    Submap "acq_ram2": address=0x80020000
    Submap "acq_ram3": address=0x80040000
    Submap "acq_exc": address=0x82000000
