.. _examples:

===========
 Examples
===========

Requirements
------------

To run examples, **PyCheby** package should be installed or available on the $PATH
(look at :ref:`quickstart`).

Moreover, the examples should be launched from the root project folder.


RF_SPS200_LOOPS
---------------

To run the python script:

.. code::

    python3 examples/rf_sps200_loops.py


First step is to get the top map from the `Tree` object:

.. code::

    tree = PyCheby('tests/maps/RF_SPS200_LOOPS/sps200CavityControl.cheby')
    topmap = tree.get()

Nodes are stored as attributes or children. The top map children are accesisble via `children` property:

.. code::

    for name, submap in topmap.children.items():
        logger.info(f'  {name:<9} = {submap}')

Attributes are associated to the memory node, e.g. for a child submap:

.. code::

    hwinfo = topmap.children['hwInfo']

    logger.info('Submap attributes:')
    logger.info(f'type(hwInfo)      = "{type(hwinfo)}"')
    logger.info(f'hwInfo.name       = "{hwinfo.name}"')
    logger.info(f'hwInfo.bus        = "{hwinfo.bus}"')
    logger.info(f'hwInfo.address    = "{hex(hwinfo.address)}"')
    logger.info('')


Attributes can be stored in containers, which are parsed as an Extension node (a special kind of memory node):

.. code::

    logger.info('HDL attribute container (stored as an Extension):')
    logger.info(f'type(hwInfo.hdl)              = "{type(hwinfo.hdl)}"')
    logger.info(f'hwInfo.hdl.name_suffix        = "{hwinfo.hdl.name_suffix}"')
    logger.info(f'hwInfo.hdl.bus_attribute      = "{hwinfo.hdl.bus_attribute}"')
    logger.info(f'hwInfo.hdl.bus_granularity    = "{hwinfo.hdl.bus_granularity}"')
    logger.info('')


Final expected output:

.. code::

    ################################################################################
    Top map "sps200CavityControl" children
    ################################################################################
      hwInfo    = SubMap: hwInfo:0:0 [MemMap: sps200CavityControl:0:0]
      sis8300ku = SubMap: sis8300ku:0x80000:0x80000 [MemMap: sps200CavityControl:0:0]
      app       = SubMap: app:0x100000:0x100000 [MemMap: sps200CavityControl:0:0]
      fgc_ddr   = SubMap: fgc_ddr:0:0 [MemMap: sps200CavityControl:0:0]
      acq_ddr   = SubMap: acq_ddr:0x20000000:0x20000000 [MemMap: sps200CavityControl:0:0]
      acq_ram   = SubMap: acq_ram:0x80000000:0x80000000 [MemMap: sps200CavityControl:0:0]
      acq_ram2  = SubMap: acq_ram2:0x80020000:0x80020000 [MemMap: sps200CavityControl:0:0]
      acq_ram3  = SubMap: acq_ram3:0x80040000:0x80040000 [MemMap: sps200CavityControl:0:0]
      acq_exc   = SubMap: acq_exc:0x82000000:0x82000000 [MemMap: sps200CavityControl:0:0]

    ################################################################################
    "hwInfo" submap attributes and children
    ################################################################################
    Submap attributes:
    type(hwInfo)      = "<class 'PyCheby.memory_nodes.SubMap'>"
    hwInfo.name       = "hwInfo"
    hwInfo.bus        = "axi4-lite-32"
    hwInfo.address    = "0x0"

    HDL attribute container (stored as an Extension):
    type(hwInfo.hdl)              = "<class 'PyCheby.memory_nodes.MemoryNode.Extension'>"
    hwInfo.hdl.name_suffix        = "_regs"
    hwInfo.hdl.bus_attribute      = "Xilinx"
    hwInfo.hdl.bus_granularity    = "byte"

    Submap enum node (stored as a list of Extensions):
    type(hwInfo.enumerations[0]) = "<class 'PyCheby.memory_nodes.MemEnum'>"

    Enum "echo":
    Enumerator unreachable     = EnumItem: unreachable [MemEnum: echo [Extension(enums)]]
    Enumerator initialized     = EnumItem: initialized [MemEnum: echo [Extension(enums)]]
    Enumerator initializing    = EnumItem: initializing [MemEnum: echo [Extension(enums)]]
    Enumerator error           = EnumItem: error [MemEnum: echo [Extension(enums)]]
    Enumerator uninitialized   = EnumItem: uninitialized [MemEnum: echo [Extension(enums)]


BI_BCT24SPS
-----------

To run the python script:

.. code::

    python3 examples/bi_bct24sps.py

Expected output:

.. code::

    ################################################################################
    Top map "BI_BCT24SPS" and its children
    ################################################################################
    Address-Space "Registers" = AddressSpace: Registers [MemMap: BI_BCT24SPS:0:0]
    Address-Space "Registers" children:
      Child "System" = SubMap: System:0:0 [AddressSpace: Registers [MemMap: BI_BCT24SPS:0:0]]
      Child "Application" = SubMap: Application:0x800000:0x800000 [AddressSpace: Registers [MemMap: BI_BCT24SPS:0:0]]

    Submap "System" and its Repeat children:
      Repeat "SysIdent": address=0x4   count=3
      Repeat "AppIdent": address=0x14  count=11

    ################################################################################
    Special-purpose INTC (Interrupt-Controller) Registers
    ################################################################################
    INTC Register "IntSource": address=0x40, access="ro"
    INTC Register "IntSource": parent="System"
    INTC Register "IntSource": full_path="BI_BCT24SPS/Registers/System/IntSource"
    INTC Register Role "IntCtrl": value="INTC_SR"
