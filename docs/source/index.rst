.. PyCheby documentation master file, created by
   sphinx-quickstart on Thu Nov  2 14:36:08 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===================================================
 PyCheby: Python library to parse Cheby memory maps
===================================================

PyCheby is a Python package for parsing Cheby memory maps, used by various tools and generators
at CERN.

The Cheby format describes the internal layout of the RF cards at CERN, and allows to generation
of various drivers and software, used by software and hardware designers. The Cheby memory map is
a "source of truth".

The Cheby memory map stores information about registers, memory blocks, addresses, offsets,
bus types, HDL, VME or PCI specific properties (and much more).

The memory map consists of:
 - memory nodes :mod:`MemoryNode <PyCheby.memory_nodes.MemoryNode>`,
 - attributes,
 - attribute containers.

Attribute containers are a standard collection of attributes or are called Memory Map
Extensions :any:`MemoryNode.Extension <PyCheby.memory_nodes.MemoryNode.Extension>`.

Attributes or Attribute Containers are always associated with a node, and describe various
properties. However, Memory Node Extensions can be stored as an attribute container or a child
of the parent node.

The **Reksio** memory map viewer can be used to edit and visualize the Cheby maps. More info
available `here <https://gitlab.cern.ch/Cheburashka/gui>`__.

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    quickstart
    examples

.. toctree::
    :caption: Documentation
    :maxdepth: 1

    api

.. toctree::
    :caption: Release Notes
    :maxdepth: 1

    release_notes/2.0.2
    release_notes/2.0.1
    release_notes/2.0.0
    release_notes/1.3.1
    release_notes/1.3.0
    release_notes/1.2.3
    release_notes/1.2.2
    release_notes/1.2.1
    release_notes/1.2.0
    release_notes/1.1.0
    release_notes/1.0.0
    release_notes/0.0.3


.. autosummary::
   :toctree: _autosummary


Contact & Support
=================

cheburashka-support@cern.ch


Indices & Tables
==================

* :ref:`genindex`
* :ref:`modindex`
