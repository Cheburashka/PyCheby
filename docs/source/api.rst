.. _api:

############################
PyCheby API Reference Manual
############################

:Release: |version|
:Date: |today|


This reference manual describers modules, classs, functions, objects included in PyCheby,
describing what they do and are used for. For learning how to use the PyCheby, see the
:ref:`quickstart <quickstart>` and :ref:`examples <examples>`.


.. rubric:: Main Modules:

.. autosummary::

    PyCheby.PyCheby
    PyCheby.memory_nodes
    PyCheby.tree
    PyCheby.utils


.. rubric:: Main Classes:

.. autosummary::

    PyCheby.PyCheby.PyCheby
    PyCheby.memory_nodes.MemoryNode
    PyCheby.memory_nodes.MemoryNode.Extension
    PyCheby.tree.TreeNode


.. toctree::
    :maxdepth: 2
    :caption: Full Contents:

    reference

