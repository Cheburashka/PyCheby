# PyCheby
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ast
import os
import setuptools
from pathlib import Path


def get_version_from_init():
    init_file = os.path.join(
        os.path.dirname(__file__), 'PyCheby', '__init__.py'
    )
    with open(init_file, 'r') as fd:
        for line in fd:
            if line.startswith('__version__'):
                return ast.literal_eval(line.split('=', 1)[1].strip())


def _read_file(fpath: str) -> str:
    return Path(fpath).read_text(encoding='utf-8')


setuptools.setup(
    name='PyCheby',
    version=get_version_from_init(),
    author='CERN (SY-RF-CS)',
    author_email='cheburashka-developers@cern.ch',
    description='A Python library to access Cheby memory maps',
    long_description=_read_file('README.rst'),
    url='https://gitlab.cern.ch/Cheburashka/PyCheby',
    packages=['PyCheby'],
    license=_read_file('LICENSE'),

    install_requires=[
        'six',
        'ruamel.yaml >= 0.18.0, <0.19.0'
    ],

    extras_require={
        'dev': [
            'pylint',
            'pycodestyle'
        ],
        'test': [],
        'docs': [
            'sphinx',
            'sphinx-rtd-theme',
        ],
        'build': [
            'setuptools >= 65.0.0, <66.0.0',
            'wheel >= 0.42.0, <0.43.0'
        ]
    },  # extras_require
)
