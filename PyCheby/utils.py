# PyCheby
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

'''
PyCheby miscellaneous utilities and helpers
'''

def align(n: int, mul: int) -> int:
    '''
    Return n aligned to the next multiple of mul
    '''
    return ((n + mul - 1) // mul) * mul


def ilog2(val: int) -> int:
    '''
    Return n such as 2**n >= val and 2**(n-1) < val
    '''
    if val & (val - 1) == 0:
        return val.bit_length() - 1
    else:
        return val.bit_length()


def round_pow2(val) -> int:
    return 1 << ilog2(val)


def str_to_int(val: str) -> int:
    '''
    String to integer converter, with support for hex numbers.
    '''
    is_hex = isinstance(val, str) and (val.find('0x') != -1)
    return int(val, 16) if is_hex else int(val)
