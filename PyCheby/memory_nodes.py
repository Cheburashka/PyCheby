# PyCheby
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

"""
This module contains all memory node classes
"""
import os
import copy

from PyCheby import utils

from enum import Enum
from .tree import TreeNode


def _process_attrs(attribs):
    '''
    Preprocess raw attributes from YAML files to PyCheby format.

    Raw attributes tags contain a hyphen "-" in the name, which should be replaced with an underscore "_". Moreover,
    the attribute containers begins with "x-" prefix which should be removed.

    Example: "x-driver-edge" -> "driver_edge" (container), "module-type" -> "module_type" (attribute).
    '''
    ret = {}
    si_suffixes = {
        'k': 2 ** 10,
        'M': 2 ** 20,
        'G': 2 ** 30
    }

    for key, value in attribs.items():
        key = key.lower()
        if key in ['size', 'repeat', 'memsize']:
            if isinstance(value, str) and value[-1] in si_suffixes and value[:-1].isdigit():
                value = int(value[:-1]) * si_suffixes[value[-1]]
        ret[key.replace('-', '_')] = value

    return ret


class MemoryNode(TreeNode):
    """
    Base class of a memory node
    """
    class Extension(TreeNode):
        '''
        Extension class for storing attribute containers or special nodes describing extra functionality.

        PyCheby Extensions (selected):

        - **driver_edge:** EDGE driver attributes in the memory map, or a special node describing
          PCI BARs for Address-Spaces,

        - **fesa:** FESA class attribute container,

        - **map_info:** memory map metainfo,

        - **hdl:** HDL-specific attributes for HDL generators.
        '''
        def _parse_extension(self, extension):
            mapping = {
                'configuration-value': ConfigurationValue,
                'constant-value': ConstantValue,
                'code-field': CodeField,
                'memory-channel': MemoryChannel,
                'pci-bar': PCIBar,
                'ip-core-description': IPCoreDescription,
                'interrupt-controller': InterruptController,
                'enum': MemEnum
            }

            if isinstance(extension, list):
                for el in extension:
                    for tag, attribs in el.items():
                        ext_cls = mapping.get(tag, MemoryNode)
                        ext_cls(parent=self, raw_attribs=attribs, **_process_attrs(attribs))
            elif isinstance(extension, dict):
                for key, value in extension.items():
                    if isinstance(value, list):
                        for element in value:
                            for tag, attribs in element.items():
                                ext_cls = mapping.get(tag, MemoryNode)
                                ext_cls(parent=self, raw_attribs=attribs, **_process_attrs(attribs))

            def _fix_attrs(attributes):
                result = {}
                if isinstance(attributes, dict):
                    for k, v in attributes.items():
                        new_key = k.replace('-', '_')
                        if isinstance(v, list):
                            pass
                        elif isinstance(v, dict):
                            result[new_key] = _fix_attrs(v)
                        else:
                            result[new_key] = v
                return result

            fixed_attrs = _fix_attrs(extension)
            return fixed_attrs

        def __init__(self, name, parent, extension):
            '''
            Extension Constructor.

            :param name: extension name.
            :type name: str
            :param parent: parent node
            :type parent: TreeNode
            :param extensions: extra special attributes of the node (defaults to None).
            :type extensions: dict, optional
            '''
            super().__init__(name, parent, _add_to_children=False)
            self.extension = self._parse_extension(extension)

        def __repr__(self):
            return f"Extension({self.name})"

        def __getitem__(self, item):
            try:
                super().__getitem__(item)
            except KeyError:
                if item in self.extension:
                    return self.extension[item]
            raise KeyError(item)

        def __getattr__(self, item):
            if item in self.__dict__:
                return self.__dict__[item]
            elif item in self.extension:
                return self.extension[item]
            elif item in self.children:
                return self.children[item]
            raise AttributeError(item)

        def __copy__(self):
            cls = self.__class__
            new_obj = cls.__new__(cls)
            new_obj.__dict__.update(self.__dict__)
            return new_obj

        def __deepcopy__(self, memo):
            cls = self.__class__
            new_obj = cls.__new__(cls)
            memo[id(self)] = new_obj
            for k, v in self.__dict__.items():
                setattr(new_obj, k, copy.deepcopy(v, memo))
            return new_obj

    def __init__(self, name: str, parent, raw_attribs, extensions=None, **kwargs):
        '''
        Memory Node Constructor.

        :param name: memory node name.
        :type name: str
        :param parent: parent node
        :type parent: TreeNode
        :param raw_attribs: raw unprocesses attributes.
        :type raw_attribs: dict
        :param extensions: extra special attributes of the node (defaults to None).
        :type extensions: dict, optional
        :param kwargs: extra keyword arguments
        :type kwargs: dict
        '''
        super().__init__(name, parent)
        self._raw_attribs = raw_attribs
        self.attribs = kwargs
        self.extensions = dict()
        if extensions is None:
            extensions = dict()

        for ext_name, ext_val in extensions.items():
            if ext_val is not None:
                self.extensions[ext_name] = self.Extension(ext_name, self, ext_val)

    def __copy__(self):
        cls = self.__class__
        new_obj = cls.__new__(cls)
        new_obj.__dict__.update(self.__dict__)
        return new_obj

    def __deepcopy__(self, memo):
        cls = self.__class__
        new_obj = cls.__new__(cls)
        memo[id(self)] = new_obj

        for k, v in self.__dict__.items():
            setattr(new_obj, k, copy.deepcopy(v, memo))
        return new_obj

    def __repr__(self):
        return "{class_name}: {name} [{parent}]".format(
            class_name=self.__class__.__name__,
            **self.__dict__,
        )

    def __getattr__(self, item):
        if item in self.__dict__:
            return self.__dict__[item]
        elif item in self.attribs:
            return self.attribs[item]
        elif item in self.extensions:
            return self.extensions[item]
        raise AttributeError(item, self)

    def walk_pre_order(self, exclude_condition=lambda node: False):
        if self.parent is None:
            yield self

        for child in self:
            if exclude_condition(child):
                continue
            yield child

        for extension in self.extensions.values():
            for child in extension:
                if exclude_condition(child):
                    continue
                yield child

        for child in self:
            if exclude_condition(child):
                continue
            yield from child.walk_pre_order(exclude_condition=exclude_condition)

        for extension in self.extensions.values():
            for child in extension:
                if exclude_condition(child):
                    continue
                yield from child.walk_pre_order(exclude_condition=exclude_condition)

    def walk_post_order(self, exclude_condition=lambda node: False):
        for child in self:
            if exclude_condition(child):
                continue
            yield from child.walk_post_order(exclude_condition=exclude_condition)

        for extension in self.extensions.values():
            for child in extension:
                if exclude_condition(child):
                    continue
                yield from child.walk_post_order(exclude_condition=exclude_condition)

        yield self

    @classmethod
    def from_loader(cls, element, **kwargs):
        """
        Standard method to parse a XML/JSON/YAML element into memory node. Calls `_process_attr()`
        on the raw attributes.

        :param element: XML/JSON/YAML element
        :type element: Any
        :param kwargs: additional arguments
        :type kwargs: dict
        :returns: processed memory node
        :rtype: MemoryNode
        """
        kwargs['raw_attribs'] = element.attrib

        extensions = {key.replace('x-', ''): value for key, value in element.attrib.items() if key.startswith("x-")}
        attributes = {key: value for key, value in element.attrib.items() if not key.startswith("x-")}

        kwargs.update({k.replace('-', '_'): v for k, v in attributes.items()})
        kwargs['extensions'] = _process_attrs(extensions)
        if issubclass(cls, Field):
            if '-' in str(attributes['range']):
                node = SubRegister(**_process_attrs(kwargs))
            else:
                node = BitField(**_process_attrs(kwargs))
        else:
            node = cls(**_process_attrs(kwargs))
        return node

    def _check_type(self, other_type, strict=False) -> bool:
        if strict:
            return type(self) == other_type  # pylint: disable=unidiomatic-typecheck
        return isinstance(self, other_type)

    def is_address_space(self, strict:bool=False) -> bool:
        """
        Is node a Address-Space?

        :param strict: if True, subclasses are not taken into account (strict type check)
        :type strict: bool
        :returns: True if is a :py:class:`Address Space <PyCheby.memory_nodes.AddressSpace>`
        :rtype: bool
        """
        return self._check_type(AddressSpace, strict)

    def is_register(self, strict:bool=False) -> bool:
        """
        Is node a register?

        :param strict: if True, subclasses are not taken into account (strict type check)
        :type strict: bool
        :returns: True if is a :py:class:`Register <PyCheby.memory_nodes.Register>`
        :rtype: bool
        """
        return self._check_type(Register, strict)

    def is_bit_field(self, strict:bool=False) -> bool:
        """
        Is node a BitField?

        :param strict: if True, subclasses are not taken into account (strict type check)
        :type strict: bool
        :returns: True if is a :py:class:`Bit Field <PyCheby.memory_nodes.BitField>`
        :rtype: bool
        """
        return self._check_type(BitField, strict)

    def is_subreg(self, strict=False):
        """
        Is node a subreg?

        :param strict: if True, subclasses are not taken into account (strict type check)
        :type strict: bool
        :returns: True if is a Sub-register
        :rtype: bool
        """
        return self._check_type(SubRegister, strict)

    def is_code_field(self, strict=False):
        """
        Is node a CodeField?

        :param strict: if True, subclasses are not taken into account (strict type check)
        :type strict: bool
        :returns: True if is a Code Field
        :rtype: bool
        """
        return self._check_type(CodeField, strict)

    def is_array(self, strict=False):
        """
        Is node an Array?

        :param strict: if True, subclasses are not taken into account (strict type check)
        :type strict: bool
        :returns: True if is a Array
        :rtype: bool
        """
        return self._check_type(Array, strict)

    def is_memory_map(self, strict=False):
        """
        Is node a MemoryMap?

        :param strict: if True, subclasses are not taken into account (strict type check)
        :type strict: bool
        :returns: True if is a Memory Map
        :rtype: bool
        """
        return self._check_type(MemMap, strict)

    def is_submap(self, strict=False):
        """
        Is node a SubMap?

        :param strict: if True, subclasses are not taken into account (strict type check)
        :type strict: bool
        :returns: True if is a Submap
        :rtype: bool
        """
        return self._check_type(SubMap, strict)

    def is_block(self, strict=False):
        """
        Is node a Block?

        :param strict: if True, subclasses are not taken into account (strict type check)
        :type strict: bool
        :returns: True if is a Block
        :rtype: bool
        """
        return self._check_type(Block, strict)

    def is_constant_value(self, strict=False):
        """
        Is node a ConstantValue?

        :param strict: if True, subclasses are not taken into account (strict type check)
        :type strict: bool
        :returns: True if is a ConstantValue
        :rtype: bool
        """
        return self._check_type(ConstantValue, strict)

    def is_configuration_value(self, strict=False):
        """
        Is node a ConfigurationValue?

        :param strict: if True, subclasses are not taken into account (strict type check)
        :type strict: bool
        :returns: True if is a ConfigurationValue
        :rtype: bool
        """
        return self._check_type(ConfigurationValue, strict)

    def is_memory_channel(self, strict=False):
        """
        Is node a memory channel?

        :param strict: if True, subclasses are not taken into account (strict type check)
        :type strict: bool
        :returns: True if is a MemoryChannel
        :rtype: bool
        """
        return self._check_type(MemoryChannel, strict)

    def is_memory_buffer(self, strict=False):
        """
        Is node a MemoryBuffer?

        :param strict: if True, subclasses are not taken into account (strict type check)
        :type strict: bool
        :returns: true if is a MemoryBuffer
        :rtype: bool
        """
        return self._check_type(MemoryBuffer, strict)

    def is_pci_bar(self, strict=False):
        """
        Is node a PCI bar?

        :param strict: if True, subclasses are not taken into account (strict type check)
        :type strict: bool
        :returns: true if is a PCI bar
        :rtype: bool
        """
        return self._check_type(PCIBar, strict)

    def is_ip_core_description(self, strict=False):
        """
        Is node a PCI Core description?

        :param strict: if True, subclasses are not taken into account (strict type check)
        :type strict: bool
        :returns: True if is a PCI core description
        :rtype: bool
        """
        return self._check_type(IPCoreDescription, strict)

    def is_interrupt_controller(self, strict=False):
        """
        Is node a Interrupt Controller?

        :param strict: if True, subclasses are not taken into account (strict type check)
        :type strict: bool
        :returns: true if is a Interrupt Controller
        :rtype: bool
        """
        return self._check_type(InterruptController, strict)

    def is_repeat(self, strict=False):
        """
        Is node a repeat node?

        :param strict: if True, subclasses are not taken into account (strict type check)
        :type strict: bool
        :returns: true if is a repeat node
        :rtype: bool
        """
        return self._check_type(Repeat, strict)

    @property
    def is_repeated_node(self):
        """
        Is node a repeated node (is one of its parents a Repeat node)?

        :returns: True if this node is repeated one
        :rtype: bool
        """
        for parent in self.parents:
            if isinstance(parent, MemoryNode) and parent.is_repeat():
                return True
        return False

    @property
    def is_repeated_multiple_times(self) -> bool:
        """
        Is node a repeated node more than once (repeat of a repeat...)

        :returns: rue if this node is multiple times repeated
        :rtype: bool
        """
        if self.is_repeat():
            repeated = not self.is_repeated_node
            return repeated
        repeats = [True for parent in self.parents if parent.is_repeat()]
        return len(repeats) > 1  # single repeat parent

    @property
    def is_first_repeated_node(self) -> bool:
        """
        If a node is a child of Repeat node and it's a first child
        Used to exclude copies and process only single child

        :returns: True if parent is Repeat instance and it's first child
        :rtype: bool
        """
        parents = self.parents
        for parent in parents:
            if isinstance(parent, MemoryNode) and parent.is_repeat():
                first_repeat_child = parent[0]
                if first_repeat_child in parents or first_repeat_child == self:
                    return True
        return False

    @property
    def unique_not_repeated(self) -> bool:
        '''
        Check whether Memory Map nodes are unique and not repeated

        :rtype: bool
        '''
        # 1) all nodes which parents are not repeats
        parents = self.parents

        no_parent_repeats = not any([parent.is_repeat() for parent in parents])
        # 2) parents can be repeats, but only when they are ALL first repeated node
        results = []
        for parent in parents:
            if parent.is_repeat():
                first_repeat_child = parent[0]
                if first_repeat_child in parents or first_repeat_child == self:
                    results.append(True)
                else:
                    results.append(False)
            else:
                results.append(True)
        return no_parent_repeats or all(results)



    def is_enum(self, strict=False):
        """
        Is node an enum?

        :param strict: if True, subclasses are not taken into account (strict type check)
        :type strict: bool
        :returns: true if is an enum node
        :rtype: bool
        """
        return self._check_type(MemEnum, strict)

    def is_enum_item(self, strict=False):
        """
        Is node an enum item?

        :param strict: if True, subclasses are not taken into account (strict type check)
        :type strict: bool
        :returns: true if is an enum item node
        :rtype: bool
        """
        return self._check_type(EnumItem, strict)

    def is_field(self, strict=False):
        """
        Is node a Field? (bit-field/sub-reg)

        :param strict: if True, subclasses are not taken into account (strict type check)
        :type strict: bool
        :returns: true if is a field node
        :rtype: bool
        """
        return self._check_type(Field, strict)

    @property
    def description(self) -> str:
        '''
        Get node's description (default empty)

        :rtype: str
        '''
        return self.attribs.get('description', '')

    @property
    def note(self) -> str:
        '''
        Get node's note (default empty)

        :rtype: str
        '''
        return self.attribs.get('note', '')

class AddressSpace(MemoryNode):
    '''
    Address Space
    '''
    def __init__(self, name, parent, **kwargs):
        '''
        Address Space Contructor.

        :param name: Address Space name.
        :type name: str
        :param parent: parent node
        :type parent: TreeNode
        :param kwargs: extra keyword arguments
        :type kwargs: dict
        '''
        super().__init__(name, parent, **kwargs)


class AddressableMemoryNode(MemoryNode):
    """
    Memory node which can be addressed
    """
    def __init__(self, name, parent, **kwargs):
        '''
        Addressable Memory Node Contructor.

        :param name: Addressable Memory Node name.
        :type name: str
        :param parent: parent node
        :type parent: TreeNode
        :param kwargs: extra keyword arguments
        :type kwargs: dict
        '''
        super().__init__(name, parent, **kwargs)
        self.offset_address = None
        self._address = None

    def __repr__(self):
        rep_str = "{class_name}: {name}:{address}:{offset}".format(
            class_name=self.__class__.__name__,
            address=hex(self.address) if self.address else self.address,
            offset=hex(self.offset_address) if self.offset_address else self.offset_address,
            **self.__dict__
        )
        if self.parent is not None:
            rep_str += " [{parent}]".format(**self.__dict__)
        return rep_str

    @property
    def address(self):
        if self._address is None:
            return self.attribs.get('address', 'next')
        else:
            return self._address

    @address.setter
    def address(self, address):
        self._address = address


class MemoryBlock(AddressableMemoryNode):
    """
    Memory node that can have elements
    """
    @property
    def arrays(self) -> list:
        """
        Get all children array nodes (top level)

        :returns: list of memory data nodes (top level children)
        :rtype: list
        """
        return [node for node in self.children.values() if node.is_array()]

    @property
    def blocks(self) -> list:
        """
        Get all children block nodes (top level)

        :returns: list of block nodes (top level children)
        :rtype: list
        """
        return [node for node in self.children.values() if node.is_block()]

    @property
    def submaps(self) -> list:
        """
        Get all submaps (top level)

        :returns: list of submaps in a memory map (top level)
        :rtype: list
        """
        return [node for node in self.children.values() if node.is_submap()]

    @property
    def registers(self) -> list:
        """
        Get all children registers (top level)

        :returns: list of registers (top level children)
        :rtype: list
        """
        return [node for node in self.children.values() if node.is_register()]

    @property
    def repeats(self) -> list:
        """
        Get all children repeats (top level)

        :returns: list of repeat nodes (top level children)
        :rtype: list
        """
        return [node for node in self.children.values() if node.is_repeat()]

    @property
    def address_spaces(self) -> list:
        '''
        Get all children Address-Spaces (top level)

        :returns: list of Address-Space nodes (top level children)
        :rtype: list
        '''
        return [node for node in self.children.values() if node.is_address_space()]

    @property
    def all_repeats(self) -> list:
        '''
        Get all children repeats

        :returns: list of repeat nodes
        :rtype: list
        '''
        return [node for node in self.walk() if node.is_repeat()]

    @property
    def pci_bar(self):
        parents = self.parents
        for parent in parents:
            if hasattr(parent, "pci_bar"):
                return parent.pci_bar
        raise Exception("No PCI bar found")


class MemMap(MemoryBlock):
    """
    Memory map main class. Root element - all sub-nodes are children.
    """
    def __init__(self, name, parent=None, **kwargs):
        '''
        Memory Map Contructor.

        :param name: memory map name.
        :type name: str
        :param parent: memory map parent node, defaults to None
        :type parent: TreeNode, optional
        :param kwargs: extra keyword arguments
        :type kwargs: dict
        '''
        super().__init__(name, parent, **kwargs)
        if kwargs.get('memmap_path', None):
            self.path, self.filename = os.path.split(kwargs['memmap_path'])

    @property
    def schema_version(self):
        return self.attribs.get('schema_version', {})

    @property
    def word_size(self):
        if self.bus == 'wb-32-be':
            return 4
        elif self.bus == 'wb-16-be':
            return 2
        elif self.bus == 'axi4-lite-32':
            return 4
        elif self.bus.startswith('cern-be-vme-'):
            return int(self.bus.split('-')[-1])//8

    @property
    def word_endian(self):
        if self.attribs.get('word_endian', None) is None:
            if self.bus == 'wb-32-be' or self.bus == 'wb-16-be':
                return 'big'
            elif self.bus == 'axi4-lite-32':
                return 'little'
            elif self.bus.startswith('cern-be-vme-'):
                return 'big'
        else:
            return self.attribs['word_endian']

    @property
    def align_registers(self) -> bool:
        return False if self.bus.startswith('cern-be-vme') else True

    @property
    def addr_width(self):
        if self.bus.startswith('cern-be-vme'):
            if self.bus.endswith('-8') or self.bus.endswith('-16'):
                return 24
            elif self.bus.endswith('-32'):
                return 32
        elif self.bus == 'wb-32-be':
            return 32
        elif self.bus == 'wb-16-be':
            return 16
        elif self.bus == 'axi4-lite-32':
            return 32
        raise Exception("Unknown bus width")

    @property
    def data_size(self):
        return self.word_size * 8

    @property
    def module_type(self):
        return self.driver_edge.module_type

    @property
    def constant_values(self) -> list:
        """
        Get all constant values. Removes duplicates.

        :returns: constant values in a memory map.
        :rtype: list
        """
        return list({node.name: node for node in self.walk() if node.is_constant_value()}.values())

    @property
    def configuration_values(self) -> list:
        """
        Get all configuration values. Removes duplicates.

        :returns: configuration values in a memory map.
        :rtype: list
        """
        return list({node.name: node for node in self.walk() if node.is_configuration_value()}.values())

    @property
    def ident_code(self):
        return self.map_info.ident

    @property
    def map_version(self):
        return self.gena.map_version

    @property
    def fesa_properties(self) -> dict:
        '''
        Get all FESA class properties such as class name, framework and class versions, creator
        name and description.

        :returns: a dictionary with FESA class metainfo.
        :rtype: dict
        '''
        return {
            'name': self.fesa.class_name,
            'framework_version': self.fesa.framework_version,
            'class_version': self.fesa.class_version,
            'description': self.fesa.description,
            'creator': self.fesa.creator
        }

    @property
    def pci_bars(self) -> list:
        ext = self.get('driver_edge')
        if ext is not None:
            return [node for node in ext.children.values() if node.is_pci_bar()]
        return []

    @property
    def ip_core_descriptions(self) -> list:
        ext = self.get('driver_edge')
        if ext is not None:
            return [node for node in ext.children.values() if node.is_ip_core_description()]
        return []

    @property
    def pci_bar(self):
        ext = self.get('driver_edge')
        if ext is None:
            return None
        return ext.get('default_pci_bar_name', None)

    @property
    def all_registers(self) -> list:
        return [node for node in self.walk() if node.is_register() and not node.parent.is_array()]

    @property
    def enumerations(self) -> list:
        ext = self.get('enums', None)
        if ext:
            return [node for node in ext.children.values() if node.is_enum()]
        return []


class SubMap(MemMap):
    """
    Memory node that represents a submap (external memory map)
    """
    def __init__(self, name, parent, **kwargs):
        '''
        Submap Contructor.

        :param name: submap name.
        :type name: str
        :param parent: parent node.
        :type parent: TreeNode
        :param kwargs: extra keyword arguments
        :type kwargs: dict
        '''
        super().__init__(name, parent, **kwargs)
        self.submap = kwargs.get('submap', None)

    @property
    def pci_bar(self):
        ext = self.get('driver_edge')
        if ext is None:
            return None
        return ext.get('pci_bar_name', None)


class Block(MemoryBlock):
    """
    This class represents an Area memory node
    """


class Array(MemoryBlock):
    """
    This class represents an array memory node
    """
    def __getattr__(self, item):
        try:
            return super().__getattr__(item)
        except AttributeError:
            # if not found, grab from the underlying register (child)
            return getattr(self.register, item)

    @property
    def element_depth(self):
        return self.c_size

    @property
    def memory_channels(self):
        return [node for node in self.walk() if node.is_memory_channel()]

    @property
    def register(self):
        return [node for node in self.children.values() if node.is_register()][0]


class Memory(Array):
    """
    This is new interface for Array (deprecated)

    .. deprecated:: 1.0.0
        Use :py:class:`Block <PyCheby.memory_nodes.Block>` instead
    """
    @property
    def repeat(self):
        return self.c_depth


class Register(AddressableMemoryNode):
    """
    This class represents single register
    """
    @property
    def read_conversion_factor(self) -> str:
        """
        Get the read conversion factor

        :returns: read conversion factor
        :rtype: str, None
        """
        ext = self.get('conversions', None)
        if ext is None:
            return None
        return ext.get('read', None)

    @property
    def write_conversion_factor(self) -> str:
        """
        Get the write conversion factor

        :returns: write conversion factor
        :rtype: str, None
        """
        ext = self.get('conversions', None)
        if ext is None:
            return None
        return ext.get('write', None)

    @property
    def has_conversion_factor(self) -> bool:
        """
        Does the register have ANY conversion factor?

        :returns: True if has ANY conversion factor
        :rtype: bool
        """
        return self.read_conversion_factor or self.write_conversion_factor

    def has_interrupt_controllers(self) -> bool:
        """
        Does the register have ANY Interrupt Controller?

        :returns: True if has ANY Interrupt Controller
        :rtype: bool
        """
        return bool(self.interrupt_controllers)

    @property
    def generate_driver(self) -> bool:
        ext = self.get('driver_edge')
        if ext is None:
            return True
        return ext.get('generate', True)

    @property
    def role_type(self) -> str:
        '''
        Register Role Type for EDGE2&3 driver. Register role is stored in a nested attribute
        container. Example:

        ::

            x-driver-edge:
                reg-role:
                    type: [IRQ_V|IRQ_L|ASSERT]

        More details:
        https://be-cem-edl.web.cern.ch/EDGE/v3.1.0/edge-schema.html#register-roles-csv-table

        :returns: register Role Type if defined, None otherwise.
        :rtype: str
        '''
        ext = self.get('driver_edge')
        if ext is None:
            return None

        role = ext.get('reg_role')
        if role is None:
            return None

        return role.get('type')

    @property
    def generate_fesa(self) -> bool:
        ext = self.get('fesa', None)
        if ext is None:
            return True
        return ext.get('generate', True)

    @property
    def subregs(self) -> list:
        """
        Get all children subregs (top level)

        :returns: subregs (top level)
        :rtype: list
        """
        return [node for node in self.children.values() if node.is_subreg()]

    @property
    def bit_fields(self) -> list:
        """
        Get all children bit fields (top level)

        :returns: bit fields (top level)
        :rtype: list
        """
        return [node for node in self.children.values() if node.is_bit_field()]

    @property
    def code_fields(self) -> list:
        """
        Get all children Code Fields

        :returns: a list of Code Fields
        :rtype: list
        """
        ext = self.get('enums')
        if ext is not None:
            return [node for node in ext.children.values() if node.is_enum_item()]
        return []

    @property
    def interrupt_controllers(self) -> list:
        """
        Get all children interrupt controllers

        :returns: a list of Interrupt Controllers
        :rtype: list
        """
        ext = self.get('driver_edge')
        if not ext:
            return []
        return [node for node in ext.children.values() if node.is_interrupt_controller()]

    @property
    def type(self) -> str:
        return self.attribs.get('type', 'unsigned')

    @property
    def maskable(self) -> bool:
        ext = self.get('gena')
        if ext is None:
            return False
        return ext.get('rmw', False)

    @property
    def multiplexed(self) -> bool:
        ext = self.get('fesa')
        if ext is None:
            return True
        return ext.get('multiplexed', True)

    @property
    def writeable(self) -> bool:
        return 'w' in self.access

    @property
    def readable(self) -> bool:
        return 'r' in self.access

    @property
    def read_only(self) -> bool:
        return self.readable and not self.writeable

    @property
    def write_only(self) -> bool:
        return self.writeable and not self.readable

    @property
    def persistence(self) -> bool:
        ext = self.get('fesa')
        if ext is None:
            return True
        return ext.get('persistence', True)

    @property
    def virtual(self) -> bool:
        return False


class Field(MemoryNode):
    '''
    This class represents Field Node
    '''
    def __getattr__(self, item):
        try:
            return super().__getattr__(item)
        except AttributeError:
            return getattr(self.parent, item)

    @property
    def code_fields(self):
        ext = self.get('enums')
        if ext is not None:
            return [node for node in ext.children.values() if node.is_enum_item()]
        return []

    @property
    def read_conversion_factor(self) -> str:
        """
        Get the read conversion factor

        :returns: read conversion factor
        :rtype: str, None
        """
        ext = self.get('conversions', None)
        if ext is not None and ext.parent == self:
            return ext.get('read', None)
        return None

    @property
    def write_conversion_factor(self) -> str:
        """
        Get the write conversion factor

        :returns: write conversion factor
        :rtype: str, None
        """
        ext = self.get('conversions', None)
        if ext is not None and ext.parent == self:
            return ext.get('write', None)
        return None

    @property
    def has_conversion_factor(self) -> bool:
        """
        Does the register have ANY conversion factor?

        :returns: True if has ANY conversion factor
        :rtype: bool
        """
        return self.read_conversion_factor or self.write_conversion_factor

    @property
    def subregs(self) -> list:
        """
        Empty property, returns []

        :returns: empty list
        :rtype: list
        """
        return []

    @property
    def bit_fields(self) -> list:
        """
        Empty property, returns []

        :return: empty list
        :rtype: list
        """
        return []


class BitField(Field):
    """
    This class represents a BitField memory node
    """
    @property
    def bit(self):
        return self.attribs.get('bit', int(self.range))

    @property
    def bit_mask(self):
        return 0x1 << self.bit

    @property
    def autoclear(self) -> bool:
        return self.get("gena.auto_clear", False)


class SubRegister(Field):
    """
    This class represents a sub register memory node
    """
    def __init__(self, **kwargs):
        '''
        Sub-Register Constructor.

        :param kwargs: a dictionary of extra key-word arguments
        :type param: dict
        '''
        super().__init__(**kwargs)
        self.msb, self.lsb = map(int, self.range.split('-'))
        self.bits = list(range(self.lsb, self.msb+1))

    @property
    def bit_mask(self) -> int:
        '''
        Calculate bit mask for bits in sub-register.
        '''
        val = 0
        for bit in self.bits:
            val |= 0x1 << bit
        return val


class CodeField(MemoryNode):
    """
    This class represents a code field memory node (single enum element)
    """
    pass


class ConstantValue(MemoryNode):
    """
    This class represents a constant value (driver level constant, i.e. the speed of light)
    """
    @property
    def data_type(self):
        """
        Get underlying data type, for example "double"

        :returns: data type, "double" by default
        """
        return self.attribs.get("type", "double")


class ConfigurationValue(MemoryNode):
    """
    This class represents a configuration value (FESA-instance-constant)
    """
    @property
    def data_type(self):
        """
        Get underlying data type, for example "double"

        :returns: data type, "double" by default
        """
        return self.attribs.get("type", "double")


class MemoryChannel(MemoryNode):
    """
    This class represents a memory channel
    """
    def __init__(self, name, parent, raw_attribs, **kwargs):
        '''
        Memory Channel Contructor.

        :param name: memory channel name.
        :type name: str
        :param parent: parent node.
        :type parent: TreeNode
        :param raw_attribs: raw unprocesses attributes.
        :type raw_attribs: dict
        :param kwargs: extra keyword arguments
        :type kwargs: dict
        '''
        memory_buffers = kwargs.pop('memory_buffers', list())
        super().__init__(name, parent, raw_attribs, **kwargs)
        for buffer in memory_buffers:
            attribs = buffer["memory-buffer"]
            MemoryBuffer(parent=self, raw_attribs=attribs, **_process_attrs(attribs))


class MemoryBuffer(MemoryNode):
    """
    This class represents a memory buffer
    """
    def __init__(self, name: str, parent, raw_attribs, **kwargs):
        '''
        Memory Buffer Contructor.

        :param name: memory buffer name.
        :type name: str
        :param parent: parent node.
        :type parent: TreeNode
        :param raw_attribs: raw unprocesses attributes.
        :type raw_attribs: dict
        :param kwargs: extra keyword arguments
        :type kwargs: dict
        '''
        bit_fields = kwargs.pop('bit-field-datas', list())
        super().__init__(name, parent, raw_attribs, **kwargs)
        for bit_field in bit_fields:
            attribs = bit_field["bit-field-data"]
            BitField(parent=self, raw_attribs=attribs, **_process_attrs(attribs))

    @property
    def buffer_id(self) -> int:
        """
        Get the buffer ID

        ::

            buffer ID
            16/32 bit int
            1/2 (8/16 bits)     1/2 (8/16 bits)
            channel id          buffer id

        :rtype: int
        """
        # different for A24/D16 and A32/D32
        shift = self.root.data_size // 2
        return (self.parent.channel_select_code << shift) + self.buffer_select_code

    @property
    def buffer_name(self):
        """
        Get the buffer name

        :returns: channel name + '_' + buffer name
        :rtype: str
        """
        return f"{self.parent.name}_{self.name}"

    @property
    def write_conversion_factor(self):
        return self.attribs.get('write_conversion_factor', None)

    @property
    def read_conversion_factor(self):
        return self.attribs.get('read_conversion_factor', None)


class PCIBar(MemoryNode):
    """
    This class represents a PCI BAR (Base Address Register)
    """
    def __init__(self, name: str, parent, raw_attribs, **kwargs):
        '''
        PCI BAR Contructor.

        :param name: PCI BAR name.
        :type name: str
        :param parent: parent node.
        :type parent: TreeNode
        :param raw_attribs: raw unprocesses attributes.
        :type raw_attribs: dict
        :param kwargs: extra keyword arguments
        :type kwargs: dict
        '''
        super().__init__(name, parent, raw_attribs, **kwargs)

        # Reksio accepts the base address as an integer or a string,
        # PyCheby should always parse the base address as an integer.
        base_addr = self.get('base_addr', None)
        if base_addr:
            self.attribs['base_addr'] = utils.str_to_int(base_addr)

class IPCoreDescription(MemoryNode):
    """
    This class represents a IP core description
    core name / version / capabilities / args
    """


class Repeat(MemoryBlock):
    """
    This class contains repetition of a single element (like submap, register...)
    """


class MemEnum(MemoryNode):
    '''
    This class represents Memory Enum
    '''
    def __init__(self, name, parent, raw_attribs, **kwargs):
        '''
         Memory Enum Contructor.

        :param name: memory enum name.
        :type name: str
        :param parent: parent node.
        :type parent: TreeNode
        :param raw_attribs: raw unprocesses attributes.
        :type raw_attribs: dict
        :param kwargs: extra keyword arguments
        :type kwargs: dict
        '''
        items = kwargs.pop('children')
        super().__init__(name, parent, raw_attribs, **kwargs)
        for item in items:
            attribs = item['item']
            processed_attribs = _process_attrs(attribs)
            # workaround for x-fesa/custom-types/enum/item
            if 'name' not in attribs:
                processed_attribs['name'] = attribs['symbol']
            EnumItem(parent=self, raw_attribs=attribs, **processed_attribs)


class EnumItem(MemoryNode):
    '''
    This class represents Enum Item
    '''
    @property
    def code(self):
        """
        To keep it compatibile with a code-field, give a value if asked for a code

        :rtype: Any
        """
        return self.get('value')


class InterruptControllerType(Enum):
    '''
    Interrupt Controller Type available for EDGE3:

    - **INTC_SR:** Status Register
    - **INTC_CR:** Control Register
    '''
    INTC_SR = 'INTC_SR'
    INTC_CR = 'INTC_CR'

class InterruptController(MemoryNode):
    """
    This class represents a Interrupt Controller (INTC) for EDGE3
    """
    def __init__(self, name, parent, raw_attribs, **kwargs):
        super().__init__(name, parent, raw_attribs, **kwargs)
        self.type = InterruptControllerType[raw_attribs['type']]
