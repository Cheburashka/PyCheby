# PyCheby
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

"""
This module contains all data parsers used on a memory map
"""
import logging

import PyCheby.utils as utils

from functools import singledispatch

from .memory_nodes import AddressableMemoryNode
from .memory_nodes import MemoryBlock
from .memory_nodes import Register
from .memory_nodes import SubMap
from .memory_nodes import Array
from .memory_nodes import Block
from .memory_nodes import MemMap
from .memory_nodes import Memory
from .memory_nodes import Repeat
from .memory_nodes import AddressSpace

LOGGER = logging.getLogger(__package__)

def set_user_size(node):
    '''
    Set node size defined by the user in the memory maps attributes
    '''
    node_size = node.attribs.get('size', None)
    if node_size is not None:
        if node_size < node.c_size:
            raise Exception(f"User provided size ({node_size}) for {node.full_name} is too small (computed: {node.c_size})!")
        node.c_size = node_size


@singledispatch
def addresser(_, __):
    '''
    Default addresser - do nothing
    '''
    pass


@addresser.register(Register)
def register_addresser(node: Register, address_calculator):
    '''
    Register Node addresser
    '''
    node.c_size = node.width // 8

    if address_calculator.align_regs:
        node.c_align = utils.align(node.c_size, address_calculator.word_size)
    else:
        node.c_align = address_calculator.word_size


@addresser.register(Block)
def block_addresser(node: Block, address_calculator):
    '''
    Block Node addresser
    '''
    if len(node):
        composite_addresser(node, address_calculator)
        set_user_size(node)
    else:
        node.c_size = node.size
    align_block(node)


@addresser.register(Array)
def array_addresser(node: Array, address_calculator):
    '''
    Array Node addresser
    '''
    composite_addresser(node, address_calculator)
    node.c_element_size = utils.align(node.c_size, node.c_align)
    if node.attribs.get('align', True):
        node.c_element_size = utils.round_pow2(node.c_element_size)
        node.c_size = node.c_element_size * node.repeat
        node.c_align = node.c_element_size * utils.round_pow2(node.repeat)
    else:
        node.c_size = node.c_element_size * node.repeat


@addresser.register(Memory)
def memory_addresser(node: Memory, address_calculator):
    '''
    Memory Node addresser
    '''
    composite_addresser(node, address_calculator)
    node.c_element_size = utils.round_pow2(node.c_size)
    node.c_depth = node.memsize // node.c_element_size
    node.c_element_size = utils.align(node.c_size, node.c_align)
    node.c_size = node.c_depth * node.c_element_size
    node.c_align = utils.round_pow2(node.c_size)
    set_user_size(node)
    align_block(node)

@addresser.register(AddressSpace)
def address_space_addresser(node: AddressSpace, address_calculator):
    '''
    Address-Space Node addresser
    '''
    composite_addresser(node, address_calculator)
    set_user_size(node)
    align_block(node)

@addresser.register(Repeat)
def repeat_addresser(node: Repeat, address_calculator):
    '''
    Repeat Node addresser
    '''
    composite_addresser(node, address_calculator)
    set_user_size(node)
    align_block(node)


@addresser.register(SubMap)
def submap_addresser(node: SubMap, address_calculator):
    '''
    Submap Node addresser
    '''
    if len(node):
        new_addresser = AddressCalculator(node)
        composite_addresser(node, new_addresser)
        set_user_size(node)

    if node.attribs.get('filename', None) is None:
        node.c_size = node.size

    align_block(node)


@addresser.register(MemMap)
def root_addresser(node: MemMap, address_calculator):
    '''
    Memory Map Node addresser - root addresser
    '''
    node.address = 0
    node.offset_address = 0
    new_addresser = AddressCalculator(node)
    composite_addresser(node, new_addresser)
    set_user_size(node)


def composite_addresser(node, address_calculator):
    '''
    Calculate address and size of the node, based on attributes and its children.
    '''
    new_calculator = address_calculator.duplicate()
    max_align = 0
    for child in node:
        if child.is_submap() and child.pci_bar is not None and child.pci_bar != address_calculator.bar:
            default_calculator = AddressCalculator(address_calculator.root, child.pci_bar)
            bar_calculator = AddressCalculator.addressers.setdefault(child.pci_bar, default_calculator)
            addresser(child, bar_calculator)
        else:
            addresser(child, new_calculator)
            max_align = max(max_align, child.c_align)

    node.c_align = max_align
    node.c_size = 0
    for child in node:
        if child.is_submap() and child.pci_bar is not None and child.pci_bar != address_calculator.bar:
            bar_calculator = AddressCalculator.addressers.get(child.pci_bar)
            bar_calculator.compute_address(child)
        else:
            new_calculator.compute_address(child)
            node.c_size = max(node.c_size, child.offset_address + child.c_size)


def align_block(node):
    '''
    Rounds up the node size to the nearest power of 2, based on the "align" attribute.
    '''
    if node.attribs.get('align', True):
        node.c_size = utils.round_pow2(node.c_size)
        node.c_align = utils.round_pow2(node.c_size)


def set_abs_address(node, base_address):
    '''
    Set absolute node offset
    '''
    node.address = base_address + node.offset_address
    if isinstance(node, Register):
        return

    if isinstance(node, (Array, Memory)):
        for child in node:
            set_abs_address(child, 0)
        return

    if isinstance(node, (MemMap, Block, SubMap, Repeat)):
        for child in node:
            set_abs_address(child, node.address)
        return

    # Address-Space's base address is fixed to 0x0 from schema-version/x-driver-edge 3.0.0+
    # Fixed 0x0 address is compliant with convention from the "cheby" tool.
    if isinstance(node, (AddressSpace)):
        node.address = 0

        # Set the same base address for each child
        for child in node:
            set_abs_address(child, 0)
        return

    raise Exception('PyCheby/data_parsers/set_abs_address: node "{node.name}" not handled')


class AddressCalculator:
    """
    This class does addressing for a memory map
    """
    addressers = {}

    def __init__(self, root, bar=None):
        self.root = root

        if root.bus.startswith('cern-be-vme-'):
            self.align_regs = False
        else:
            self.align_regs = True

        self.word_size = root.word_size
        self.address = 0
        if bar is None:
            self.bar = min(self.root.pci_bars, key=lambda bar: bar.number).name if self.root.pci_bars else 'bar0'
        else:
            self.bar = bar
        self.addressers[self.bar] = self

    def duplicate(self):
        calculator = AddressCalculator(self.root)
        calculator.align_regs = self.align_regs
        return calculator

    def compute_address(self, node):
        '''
        Compute node address
        '''
        if node.attribs.get('address', None) is None or node.attribs.get('address') == 'next':
            self.address = utils.align(self.address, node.c_align)
        else:
            self.address = node.attribs.get('address')
        node.offset_address = self.address
        self.address += node.c_size

    def addresser(self, node):
        return addresser(node, self)

    def execute(self):
        """
        Does the addressing for a memory map
        """
        root_node = self.root
        self.addresser(root_node)
        set_abs_address(root_node, 0)


class AccessModeFlipper:
    """
    This class will flip access modes, based on x-gena/access-mode-flip and x-gena/ro2rw

    """
    access_flip_mapping = {
        "rw": "ro",
        "wo": "ro",
        "ro": "rw"
    }
    ro2wo_mapping = {
        "rw": "ro",
        "wo": "ro",
        "ro": "wo"
    }

    def __init__(self, root_node):
        self.root = root_node

    def execute(self):
        submaps = [submap for submap in self.root.walk() if submap.is_submap()]

        for submap in submaps:
            access_mode_flip = submap.get("gena.access_mode_flip", False)
            ro2wo = submap.get("gena.ro2wo", False)
            if access_mode_flip:
                mapping = self.access_flip_mapping
                if ro2wo:
                    mapping = self.ro2wo_mapping
            else:
                mapping = None

            if mapping is not None:
                for node in submap.walk():
                    if node.is_register():
                        node.access = mapping[node.access]


class EnumResolver:
    """
    This class will resolve enumerations to keep the same interface as for code-fields
    """

    def __init__(self, root_node):
        self.root = root_node

    def execute(self):
        regs_fields = [node for node in self.root.walk() if node.is_register() or node.is_field()]
        for node in regs_fields:
            enums_ext = node.get('enums', None)
            if enums_ext is not None and enums_ext.parent == node:
                map_parent = next(filter(lambda node: node.is_submap() or node.is_memory_map(), node.parents))
                enum_name_ref = enums_ext.extension['name']

                # get the enum from parent
                enum_ref = [node for node in map_parent.enumerations if node.name == enum_name_ref]
                if len(enum_ref) != 1:
                    raise Exception("Enumeration not found")

                enum_ref = enum_ref[0]
                enums_ext.children = enum_ref.children
