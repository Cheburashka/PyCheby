# PyCheby
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

"""
This module provides basic class for a  memory node - tree interface
"""
import re
import logging

from collections import OrderedDict
from collections.abc import Sequence
from enum import Enum
from functools import lru_cache

LOGGER = logging.getLogger(__package__)

def _is_address_space(node) -> bool:
    """
    Generic check that the node is an AddressSpace.
    """
    getter_name = 'is_address_space'
    is_callable = hasattr(node, getter_name) and callable(getattr(node, getter_name))
    return is_callable and node.is_address_space()


class TreeNode:
    """
    Tree-node base class
    """
    class TraverseType(Enum):
        """
        This enum is used to define traverse type for search function in a tree.

        - **POST_ORDER:** option is especially useful while procesing the tree for C++,
          when all children have to be processed before the parent,

        - **PRE_ORDER:** useful when searching for a node.
        """
        POST_ORDER = 0
        PRE_ORDER = 1

    def __init__(self, name, parent, _add_to_children=True):
        '''
        TreeNode constructor.

        :param name: node name.
        :type name: str
        :param parent: parent node, or None if this is the root node.
        :type parent: TreeNode or None
        :param _add_to_children: if true, add this node to the parent's children (defaults: True).
        :type _add_to_children: bool, optional
        '''
        self.name = name
        self.parent = parent
        self.children = OrderedDict()
        self._full_name_list = None
        if parent is not None and _add_to_children:
            parent[self.name] = self

    def __getitem__(self, item):
        # used by Gena
        if isinstance(item, int):
            return list(self.children.values())[item]
        elif item in self.children:
            return self.children[item]
        raise KeyError(item)

    def get(self, item, default=None):
        '''
        Get a nested attribute from an object.

        :param item: attribute or key to retrieve, specified as a dot-separated string.
        :type item: str
        :param default: value to return if the attribute or key does not exist, defaults to None.
        :type default: Any, optional
        :returns: retrieved attribute or key, or the default value if it does not exist.
        :rtype: Any
        :raises AttributeError: if an attribute does not exist.
        :raises KeyError: if a dictionary key does not exist.

        Example:

        >>> tree = PyCheby('tests/maps/RF_SPS200_LOOPS/sps200CavityControl.cheby')
        >>> tree.root_node.get("bus")
        "axi4-lite-32"
        >>> tree.root_node.get('driver_edge.module_type')
        "RF_SPS200_LOOPS"
        >>> tree.root_node.get("non_existent_attribute", "default_value")
        "default_value"
        '''
        separator = '.'
        args = item.split(separator)
        res = self
        try:
            for arg in args:
                if isinstance(res, dict):
                    res = res[arg]
                else:
                    res = getattr(res, arg)
        except (AttributeError, KeyError):
            return default

        return res

    def __setitem__(self, item, value):
        self.children[item] = value

    def __delitem__(self, item):
        obj = self.children.pop(item)
        obj.parent = None
        del obj

    def __len__(self):
        return len(self.children)

    def __iter__(self):
        return iter(self.children.values())

    def walk(self, traverse_type=TraverseType.POST_ORDER, exclude_condition=lambda node: False):
        """Generator-flattener of memory map.
        ::

                          +---+
                          | A |
                          --+--
                            |
                            |
              +--------+----+---+--------+
              |        |        |        |
            +-+-+    +-+-+    +-+-+    +-+-+
            | B |    | C |    | D |    | E |
            +---+    +---+    +---+    +-+-+
                                         |
                                       +-+-+
                                       | F |
                                       +-+-+
                                         |
                                    +----+---+
                                    |        |
                                  +-+-+    +-+-+
                                  | G |    | H |
                                  +---+    +-+-+
                                             |
                                         +---+----+
                                         |        |
                                       +-+-+    +-+-+
                                       | I |    | J |
                                       +---+    +---+
            Output for node=A:
                traverse_type=TraverseType.POST_ORDER:
                B, C, D, G, I, J, H, F, E, A
                traverse_type=TraverseType.PRE_ORDER:
                A, B, C, D, E, F, G, H, I, J

        :py:class:`POST_ORDER<TraverseType>` option is especially useful while procesing
        the tree for C++, when all children have to be processed before the parent. On the contrary,
        :py:class:`PRE_ORDER<TraverseType>` is useful when searching for a node.

        :param exclude_condition: any callable with single arg (node)
            based on this function result, some nodes can be excluded
        :type exclude_condition: typing.Callable
        :param traverse_type: TraverseType enum (POST_ORDER or PRE_ORDER)
        :type traverse_type: TraverseType
        :return: TreeNode element
        :rtype: TreeNode
        :raises: TypeError: if :py:class:`TraverseType` enumerator is wrong.
        """
        if traverse_type == TreeNode.TraverseType.POST_ORDER:
            yield from self.walk_post_order(exclude_condition=exclude_condition)
        elif traverse_type == TreeNode.TraverseType.PRE_ORDER:
            yield from self.walk_pre_order(exclude_condition=exclude_condition)
        else:
            raise TypeError("Wrong traverse_type provided.")

    def walk_pre_order(self, exclude_condition=lambda node: False):
        """
        Generator-flattener of memory map, pre-order. See :py:meth:`walk()<TreeNode.walk>` method.

        :param exclude_condition: any callable taking the node as input, and returning boolean.
            Based on the callable result, the node can be excluded.
        :type exclude_condition: Callable
        """
        if self.parent is None:
            yield self
        for child in self:
            if exclude_condition(child):
                continue
            yield child
        for child in self:
            if exclude_condition(child):
                continue
            yield from child.walk_pre_order(exclude_condition=exclude_condition)

    def walk_post_order(self, exclude_condition=lambda node: False):
        """
        Generator-flattener of memory map, post-order. See :py:meth:`walk()<TreeNode.walk>` method.

        :param exclude_condition: any callable taking the node as input, and returning boolean.
            Based on the callable result, the node can be excluded.
        :type exclude_condition: Callable
        """
        for child in self:
            if exclude_condition(child):
                continue
            yield from child.walk_post_order(exclude_condition=exclude_condition)
        yield self

    @property
    def root(self):
        """
        Returns the root element for a memory map.

        :returns: root node of a memory map
        :rtype: MemoryNode
        """
        if self.parent is None:
            return self

        parent = self.parent
        while getattr(parent, 'parent', None):
            parent = parent.parent

        return parent

    def change_children_parent(self, new_parent):
        """
        Change parent of a node. Also updates children of a node.

        :param new_parent: a new parent node.
        :type new_parent: MemoryNode
        """
        for child in self.children.values():
            child.parent = new_parent
        new_parent.children.update(self.children)
        self.children = OrderedDict()


    @property
    def full_name_list(self):
        """
        Iterates through node's parents to get a list of names, starting from root.
        Caches the result. :py:class:`Address Space <PyCheby.memory_nodes.AddressSpace>`
        node's name is transparent and not taken into account (backward compatibility).

        :returns: a list of names of all parent nodes.
        :rtype: list
        """
        if self._full_name_list is None:
            parent = self.parent
            name = []
            name.insert(0, self.name)

            # Skip Address-Spaces to keep backward compatibility
            #
            # IMPORTANT NOTE:
            # Extra safety check is required to verify that the parent is an Address-Space,
            # since not every parent node is a 'MemoryNode', and it cannot be assumed,
            # that the 'is_address_space()' getter is always available.
            #
            # E.g. Configuration-Field's parent type is MemoryNode.Extension, which has
            # no method 'is_address_space()'. The same applies to any node which parent
            # type is 'MemoryNode.Extension'.
            while getattr(parent, 'parent', None):
                if not _is_address_space(parent):
                    name.insert(0, parent.name)
                parent = parent.parent
            self._full_name_list = name

        return self._full_name_list

    def rel_name_list(self, parent_node) -> list:
        '''
        Gets all relative nodes to the provided parent node, and returns as a list.
        :py:class:`Address Space <PyCheby.memory_nodes.AddressSpace>` node's name is transparent
        and not taken into account (backward compatibility).

        :param parent_node: a reference parent node to the current node.
        :type parent_node: MemoryNode
        :returns: a list of names of all relatives nodes to the provided parent node.
        :rtype: list
        '''
        parent = self.parent
        name = []
        name.insert(0, self.name)

        if self == parent_node:
            return name

        while getattr(parent, 'parent', None):
            if parent == parent_node:
                break
            if not _is_address_space(parent):
                name.insert(0, parent.name)
            parent = parent.parent
        return name

    @property
    def full_name(self) -> str:
        """
        Gets the parents' names into string. Uses '_' as separator.

        :param separator: separator between nodes' names
        :type separator: str
        :returns: parents names from the root node as a string
        :rtype: str
        """
        return self.get_full_name(separator='_')

    def get_full_name(self, separator:str = '_') -> str:
        """
        Gets the parents' names into string.

        :param separator: separator between nodes' names
        :type separator: str
        :returns: parents names from the root node as a string
        :rtype: str
        """
        return separator.join(self.full_name_list)

    def get_full_path(self, separator:str = '/') -> str:
        """
        Gets full node path into string, from the root node

        :param separator: separator between nodes' names
        :type separator: str
        :returns: full node path from the root node as a string
        :rtype: str
        """
        tmp = separator.join(parent.name for parent in reversed(self.parents))
        return tmp + separator + self.name

    def node_lookup(self, name, sep:str = '_'):
        """
        Looks for a node with given name (can be relative or absolute, separated by sep).
        By default, it looks for a node matching name among children of given node. If not found,
        re-starts from a level above (parent of a given node).
        If a list with names is provided, will return first found or none.

        :param name: name of the node, or list of str names (str or list[str] (Sequence(str))).
        :type name: str, list, Sequence
        :param sep: separator between memory node's parents
        :type sep: str, optional
        :return: the first node matching the name, or None if no match or such node does not exists.
        :rtype: MemoryNode or None
        :raises TypeError: if the name parameter is neither a string nor a sequence.
        """
        if not name:
            return None

        if isinstance(name, str):
            return self.node_lookup_single(name, sep)

        if not isinstance(name, Sequence):
            raise TypeError(f"Either str or a Sequence expected")

        # iterate over names
        for node_name in name:
            node = self.node_lookup_single(node_name, sep)
            if node is not None:
                return node
            LOGGER.debug(f"Memory node matching {node_name} was not found")

        LOGGER.error(f"Memory node matching any of {name} was not found.")
        return None

    @lru_cache(maxsize=32)
    def node_lookup_single(self, name, sep:str = '_', _going_up_permitted:bool = True):
        """
        Looks for a node with given name (can be relative or absolute, separated by sep).
        By default, it looks for a node matching name among children of given node. If not found,
        re-starts from a level above (parent of a given node).

        :param name: name of a node to find, a string separated by the separator argument,
            representing a relative/absolute path (internally splitted to a list).
        :type name: str
        :param sep: separator character used in the node name (defaults to '_').
        :type sep: str, optional
        :param _going_up_permitted: internally used. If True, the lookup can be done
            in node's parent (and higher).
        :type _going_up_permitted: bool, optional
        :return: node matching the name, or None if no match or such node does not exists.
        :rtype: Node or None
        """
        if not name:
            return None

        name = str.split(name, sep)
        # look among children
        element = next((node for node in self.walk_pre_order() if str.lower(node.name) == str.lower(name[0])), None)

        if element is None and _going_up_permitted:
            # not found among children
            # lets go up
            if self.parent is None:
                return None  # cannot go up...
            return self.parent.node_lookup_single(sep.join(name), sep)
        elif len(name) > 1:
            # root of the element that we're looking for is found.
            # lets look for the rest
            return element.node_lookup_single(sep.join(name[1:]), sep, _going_up_permitted=False)

        return element

    def node_lookup_regex(self, pattern: str, sep:str = '_', match_full:str = False) -> list:
        '''
        Goes through all memory nodes and something matches a pattern then returns it.

        :param pattern: the regex pattern.
        :type pattern: str
        :param sep: separator between memory node's parents.
        :type sep: str, optional
        :param match_full: should the pattern be used on full name of a memory node (with parents)
            or just on its name.
        :type match_full: bool, optional
        :returns: a list of matching elements.
        :rtype: list
        :raises re.error: If the pattern is not a valid regex.
        '''
        try:
            pattern = re.compile(pattern)
        except re.error as exception:
            LOGGER.error(f"The pattern: '{pattern}' is wrong: {exception}")
            return []

        if match_full:
            return [node for node in self.walk() if pattern.match(node.get_full_name(sep))]

        return [node for node in self.walk() if pattern.match(node.name)]

    @property
    def parents(self) -> list:
        '''
        Gets all node parents and return as a list

        :returns: a list of all node's parents.
        :rtype: list
        '''
        parents = []
        parent = self.parent
        if parent is None:
            return parents

        while parent:
            parents.append(parent)
            parent = parent.parent

        return parents
