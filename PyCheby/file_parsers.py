# PyCheby
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

"""
This module contains XML/JSON/YAML loaders and savers
"""
import os
import json
import ruamel.yaml as yaml

from functools import partial
from xml import etree

from .memory_nodes import MemoryNode
from .memory_nodes import SubMap


class Loader:
    """
    Base class for a specific file loader
    """
    library = None
    extensions = []

    class ElementWrapper:
        """
        Helper class to wrap an element so it has the same interface as in XML
        """
        def __init__(self, root):
            self.tag = list(root.keys())[0]
            self.root = root[self.tag]
            self.idx = 0
            self.get = self.root.get
            self.attrib = {k: v for k, v in self.root.items() if 'children' not in k}

        def __iter__(self):
            return self

        def __next__(self):
            try:
                if 'children' in self.root:
                    item = Loader.ElementWrapper(self.root['children'][self.idx])
                    self.idx += 1
                    return item
            except IndexError:
                self.idx = 0
            raise StopIteration

        def __len__(self):
            return len(getattr(self.root, 'children', []))

    def __init__(self, path):
        self.root = self._get_document(path)

    def getroot(self):
        """
        Gets the root element
        Returns
        -------
        root element
        """
        return self.root

    def _get_document(self, path):
        library = __import__(self.library)
        with open(path, 'r') as f:
            document = library.load(f)
        return Loader.ElementWrapper(document)


class XMLLoader(Loader):
    """
    Loader class for XML files
    """
    extensions = ['.xml']

    def __init__(self, path):
        super().__init__(path)
        xmls = '{http://www.w3.org/2001/XMLSchema-instance}schemaLocation'
        if xmls in self.getroot().attrib:
            del self.getroot().attrib[xmls]

    def _get_document(self, path):
        parser = etree.XMLParser(remove_comments=True)
        return etree.parse(path, parser=parser).getroot()


class JSONLoader(Loader):
    """
    Loader class for JSON files
    """
    extensions = ['.json']
    library = 'json'


class YAMLLoader(Loader):
    """
    Loader class for YAML files
    """
    extensions = ['.yaml', '.yml', '.cheby']
    library = 'ruamel.yaml'

    def _get_document(self, path):
        with open(path, 'r') as f:
            document = yaml.YAML(typ='safe').load(f)
        return Loader.ElementWrapper(document)


class Saver:
    """
    Saver base class - writes the memory map to a file
    """
    extension = None

    def __init__(self, memmap):
        self.memmap = memmap
        self.dumps = None

    def save(self, path):
        """
        Saves memory map to given file
        Parameters
        ----------
        path: str
            path to a file
        """
        with open(path, 'w') as f:
            f.write(self.dumps)


class JSONSaver(Saver):
    """
    Saves memory map to a JSON file
    """
    extension = '.json'

    class Encoder(json.JSONEncoder):
        """
        Custom json's Encoder to process memory nodes
        """
        caller = None

        def default(self, o):  # pylint: disable=method-hidden
            if isinstance(o, MemoryNode):
                attrs = o._raw_attribs  # pylint: disable=protected-access
                if o.is_submap():
                    attrs = attrs['submap']
                    jsonsaver = self.caller(o, _inside_submap=True)  # pylint: disable=not-callable
                    filename, _ = os.path.splitext(o.filename)
                    o.filename = filename + self.caller.extension
                    jsonsaver.save(o.filename)
                    return {o.type: {**attrs}}
                return {o.type: {**attrs, 'children': list(o.children.values())}}
            return json.JSONEncoder.default(self, o)

    class SubmapEncoder(Encoder):
        """
        Specialized Encoder for handling submaps
        """
        def default(self, o):  # pylint: disable=method-hidden
            default_encoder = partial(JSONSaver.Encoder.default, self)
            if isinstance(o, SubMap):
                tag = 'memory-map'
                attrs = o._raw_attribs['memmap']  # pylint: disable=protected-access
                return {tag: {**attrs, 'children': list(map(default_encoder, o.children.values()))}}
            return default_encoder(o)

    def __init__(self, memmap, _inside_submap=False):
        super().__init__(memmap)
        encoder = self.Encoder if not _inside_submap else self.SubmapEncoder
        encoder.caller = self.__class__
        self.dumps = json.dumps(self.memmap, cls=encoder, indent=4)


class YAMLSaver(JSONSaver):
    """
    Saves memory map to a YAML file
    """
    extension = '.yaml'

    def __init__(self, memmap, _inside_submap=False):
        super().__init__(memmap, _inside_submap=_inside_submap)
        self.dumps = yaml.dump(yaml.load(self.dumps))


class XMLSaver(Saver):
    """
    Saves memory map to a XML file
    """
    extension = '.xml'

    def __init__(self, memmap, _inside_submap=False):
        super().__init__(memmap)
        self.dumps = etree.ElementTree(self._create_xml(memmap, _inside_submap=_inside_submap))
        self.save = partial(self.dumps.write, pretty_print=True, xml_declaration=True, encoding='UTF-8')

    def _create_xml(self, node, parent=None, _inside_submap=False):
        tag = node.type
        attrs = node._raw_attribs  # pylint: disable=protected-access
        children = list(node.children.values())

        if _inside_submap:
            # treat as a memory map
            tag = 'memory-map'
            attrs = attrs['memmap']
        elif node.is_submap():
            # treat as a submap
            attrs = attrs['submap']
            memmap = XMLSaver(node, _inside_submap=True)
            filename, _ = os.path.splitext(node.filename)
            node.filename = filename + self.extension
            memmap.save(node.filename)
            children = list()  # children are processed in XMLSaver call above.

        parent = etree.Element(tag, attrib=attrs) if parent is None else etree.SubElement(parent, tag, attrib=attrs)
        for child in children:
            self._create_xml(child, parent)
        return parent


class FileParser:
    """
    Helper class to extract the extension from given path and delegate save/load methods to proper class
    """
    parsers = {}
    savers = {}

    @staticmethod
    def get_parser(path: str):
        """
        Gives the proper loader class to handle with memory map's format
        Parameters
        ----------
        path: str
            path to a memory map
        Returns
        -------
        Loader
            loader specialized in given extension (format)
        """
        if not path:
            raise FileNotFoundError('FileParser: input path cannot be empty!')

        _, extension = os.path.splitext(path)

        if extension not in FileParser.parsers.keys():
            raise KeyError(f"FileParser: file exension '{extension}' not supported (Accepted: {FileParser.parsers.keys()})")

        return FileParser.parsers[extension](path)

    @staticmethod
    def save_map(mem_map, path: str):
        """
        Save given memory map in path
        Parameters
        ----------
        mem_map
            memory map root node
        path: str
            output path to save the map
        """
        _, extension = os.path.splitext(path)
        FileParser.savers[extension](mem_map).save(path)

    @staticmethod
    def register(loader: Loader):
        """
        Register a loader
        Parameters
        ----------
        loader: Loader
            loader to register
        """
        if hasattr(loader, 'extensions'):
            for extension in loader.extensions:
                FileParser.parsers[extension] = loader
        else:
            raise TypeError

    @staticmethod
    def register_saver(saver: Saver):
        """
        Register a saver (with all extensions it can handle)
        Parameters
        ----------
        saver: Saver
            saver to register
        """
        if hasattr(saver, 'extension'):
            FileParser.savers[saver.extension] = saver


FileParser.register(XMLLoader)
FileParser.register(YAMLLoader)
FileParser.register(JSONLoader)
FileParser.register_saver(XMLSaver)
FileParser.register_saver(YAMLSaver)
FileParser.register_saver(JSONSaver)
