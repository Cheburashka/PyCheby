# PyCheby
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

"""
This module contains simple factory to build memory node from XML/JSON/YAML node
"""
from .memory_nodes import MemMap
from .memory_nodes import SubMap
from .memory_nodes import Register
from .memory_nodes import Block
from .memory_nodes import BitField
from .memory_nodes import CodeField
from .memory_nodes import SubRegister
from .memory_nodes import Array
from .memory_nodes import Field
from .memory_nodes import ConstantValue
from .memory_nodes import ConfigurationValue
from .memory_nodes import MemoryChannel
from .memory_nodes import MemoryBuffer
from .memory_nodes import MemoryNode
from .memory_nodes import Memory
from .memory_nodes import Repeat
from .memory_nodes import AddressSpace


class MemoryNodeFactory:
    """
    Factory to build memory nodes
    """
    @staticmethod
    def from_loader(element, parent=None, **kwargs):
        """
        This method is called directly by PyCheby class to build a memory map and it calls recursively itself.
        Parameters
        ----------
        element
            XML/JSON/YAML/... element
        parent
            parent of tne memory node (None if root element)
        kwargs
            keywords to pass, like 'type' for element tag
        Returns
        -------
            memory node object
        """
        mapped_type = MemoryNodeFactory.get_mapped_type(element.tag)
        obj = mapped_type.from_loader(element, parent=parent, **kwargs)
        for child in element:
            MemoryNodeFactory.from_loader(child, parent=obj, **kwargs)
        return obj

    @staticmethod
    def get_mapped_type(tag):
        '''
        YAML/XML/JSON Tag to Memory Node converter.
        '''
        mapped_type = MemoryNodeFactory.tag_node_mapping.get(tag, MemoryNode)
        return mapped_type

    tag_node_mapping = {
        'memory-map': MemMap,
        'submap': SubMap,
        'reg': Register,
        'block': Block,
        'array': Array,
        'memory': Memory,
        'field': Field,
        'repeat': Repeat,
        'address-space': AddressSpace
    }
