# PyCheby
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

"""
PyCheby module
"""
import logging
__version__ = '2.0.2'

# Default logging handler set to Null to avoid problems in Windows GUIs applications.
# In some situations, stdin, stderr and stdout can be set to None, and this is often
# the case for Windows GUIs (e.g. Reksio at CERN) not connected to any console.
# The same case applies to pythons apps using "pythonw" on Windows.
logging.getLogger(__name__).addHandler(logging.NullHandler())
