# PyCheby
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

"""
Main module with PyCheby class
"""
import os
import copy
import logging

from .file_parsers import FileParser
from .factory import MemoryNodeFactory

from .data_parsers import AddressCalculator
from .data_parsers import AccessModeFlipper
from .data_parsers import EnumResolver

from .memory_nodes import SubMap
from .memory_nodes import _process_attrs
from .memory_nodes import Repeat


def repeat_from_loader(cls, element, **kwargs):
    """
    Standard method to parse a XML/JSON/YAML element into memory node. Calls _process_attr on raw attribs
    Parameters
    ----------
    element
        XML/JSON/YAML element
    kwargs
        additional arguments
    Returns
    -------
    MemoryNode
        processed memory node
    """
    kwargs['raw_attribs'] = element.attrib

    extensions = {key.replace('x-', ''): value for key, value in element.attrib.items() if key.startswith("x-")}
    attributes = {key: value for key, value in element.attrib.items() if not key.startswith("x-")}

    kwargs.update({k.replace('-', '_'): v for k, v in attributes.items()})
    kwargs['extensions'] = _process_attrs(extensions)
    node = cls(**_process_attrs(kwargs))

    ignore_version_check = kwargs.get('ignore_version_check')

    count = kwargs.get('count')
    single_element = None
    for child in element:
        # pass parent and ignore_version_check as they're needed for instance for a repetition of a submap
        single_element = MemoryNodeFactory.from_loader(child, parent=node, ignore_version_check=ignore_version_check)

    # single_element is now a child of node, remove it
    node.children.clear()

    # lets rename single element
    for i in range(count):
        child_name = f"{single_element.name}_{i}"
        child = copy.deepcopy(single_element)
        child.parent = node
        child.name = child_name
        node[child_name] = child

    # reset children as they are already processed
    element.root['children'] = []
    return node


Repeat.from_loader = classmethod(repeat_from_loader)


def submap_from_loader(cls, element, **kwargs):
    """
    This class method is used to call PyCheby on external file (submap) and store both sets of attributes:
    from submap tag and from memory-map tag (external file)
    Those attributes are stored as usual in 'raw_attribs' dictionary, in 'submap' or 'memmap' key.
    Parameters
    ----------
    cls
        class instance of memory node object (submap in this case)
    element
        XML/YAML/JSON element to read the data from
    kwargs
        other arguments passed by the factory (parent, type (element's tag) for instance)
    Returns
    -------
    SubMap
        processed submap object
    """
    def merge(source, destination):
        for k, v in source.items():
            if isinstance(v, dict):
                node = destination.setdefault(k, {})
                merge(v, node)
            else:
                if k not in destination:
                    destination[k] = v
        return destination

    submap_raw_attribs = element.attrib

    extensions = {key.replace('x-', ''): value for key, value in element.attrib.items() if key.startswith("x-")}
    attributes = {key: value for key, value in element.attrib.items() if not key.startswith("x-")}

    submap_attribs = _process_attrs(attributes)
    kwargs['extensions'] = _process_attrs(extensions)

    parent = kwargs.get('parent')
    ignore_version_check = kwargs.get('ignore_version_check')

    filename = submap_attribs.get('filename')

    if filename:
        if not os.path.isabs(filename):
            filename = os.path.normpath(os.path.join(parent.root.path, filename))

        memmap = PyCheby(filename, parent=None, ignore_version_check=ignore_version_check).get(parse=False)

        kwargs['raw_attribs'] = {
            'submap': submap_raw_attribs,
            'memmap': memmap._raw_attribs
        }
        kwargs['memmap_extensions'] = memmap.extensions
        memmap.attribs.update(submap_attribs)
        kwargs.update(memmap.attribs)
        kwargs['memmap_path'] = filename
        submap = cls(submap_raw_attribs=submap_raw_attribs, **kwargs)
        memmap.change_children_parent(submap)

        for map_extension_name, map_extension in memmap.extensions.items():
            # update the parent
            map_extension.parent = submap
            if map_extension_name in submap.extensions:
                submap_extension = submap.extensions[map_extension_name]
                merge(map_extension.extension, submap_extension.extension)
                submap_extension.children.update(map_extension.children)
            else:
                submap.extensions[map_extension_name] = map_extension

    else:
        kwargs['raw_attribs'] = submap_raw_attribs
        kwargs.update({k.replace('-', '_'): v for k, v in submap_attribs.items()})
        submap = cls(**kwargs)

    return submap


SubMap.from_loader = classmethod(submap_from_loader)

LOGGER = logging.getLogger(__package__)

class PyCheby:
    """
    This class loads memory maps and parses them
    """
    actions = [
        AddressCalculator, # calculate addressing
        AccessModeFlipper, # flip access mode
        EnumResolver # resolve enumerations to code-fields
    ]

    MIN_SCHEMA_VERSIONS = {
        'core': '3.0.0',
        'x-map-info': '1.0.0',
        'x-fesa': '2.0.0',
        'x-driver-edge': '3.0.0'
    }

    def __init__(self, memmap_path: str, parent=None, validate=None, ignore_version_check=None):
        '''
        Class Constructor

        :param memmap_path: a path to memory map
        :type memmap_path: str
        :param parent: parent node
        :type parent: TreeNode
        :param validate: validation flag for the memory map
        :type validate: bool
        :param ignore_version_check: if enabled, minimum required schema version check is disabled.
            Parser may work for older schemas but there is no warranty.
        :type ignore_version_check: bool
        '''
        self.validate = validate or False
        self.path, self.filename = os.path.split(memmap_path)

        loader = FileParser.get_parser(memmap_path)
        root = loader.getroot()

        self.root_node = MemoryNodeFactory.from_loader(root,
                                                       parent,
                                                       memmap_path=memmap_path,
                                                       ignore_version_check=ignore_version_check)
        self._parsed = False

        if ignore_version_check:
            return

        self.check_schema_version()

    def get(self, parse=True):
        """
        This method returns root memory map node.
        Parameters
        ----------
        parse: bool
            if true, then parse memory map
        Returns
        -------
        memory map's root node
        """
        if not self._parsed and parse:
            self.parse(self.root_node)
        return self.root_node

    def parse(self, node):
        """
        Parse recursively given memory node.
        Only the first call is effective. To parse again, set False to self._parsed
        Parameters
        ----------
        node
            memory node to parse (usually root memory node)
        """
        for action in self.actions:
            a = action(node)
            a.execute()
        self._parsed = True

    def check_schema_version(self):
        '''
        Check whether the schema version of the memory map is compatible with the minumum required version
        '''
        schema_versions = self.root_node.schema_version
        validation_okay = True
        error_msg = ""
        for extension, min_version in PyCheby.MIN_SCHEMA_VERSIONS.items():

            min_major, min_minor, min_patch = map(int, min_version.split('.'))

            current_ext_version = schema_versions.get(extension, None)

            if current_ext_version is None:
                validation_okay = False
                error_msg += f"Error when parsing memory map {self.filename} ( {self.path} ):\n" \
                             f"Missing memory map schema version for \"{extension}\": expected {min_version}\n"
            else:
                curr_major, curr_minor, curr_patch = map(int, current_ext_version.split('.'))
                if curr_major < min_major:
                    error_msg += f"Memory map schema version for \"{extension}\" too low: expected {min_version}, got {current_ext_version}\n"
                    validation_okay = False
                if curr_major > min_major:
                    error_msg += f"Memory map schema version for \"{extension}\" too high: expected {min_version}, got {current_ext_version}"
                    validation_okay = False

        if not validation_okay:
            raise Exception(f"Memory map schema is not compatibile with this tool:\n{error_msg}\n"
                            f"Please use Reksio to upgrade your memory map, or use 'ignore_version_check' flag to disable version checking.")
