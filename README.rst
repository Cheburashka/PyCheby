.. image:: https://gitlab.cern.ch/Cheburashka/PyCheby/badges/master/pipeline.svg
    :target: https://gitlab.cern.ch/Cheburashka/PyCheby/commits/master
    :alt: build status


.. image:: https://gitlab.cern.ch/Cheburashka/PyCheby/-/badges/release.svg?order_by=release_at
    :target: https://gitlab.cern.ch/Cheburashka/PyCheby/-/releases?order_by=release_at
    :alt: latest release

=======
PyCheby
=======

PyCheby is a Python package to parse Cheby memory maps, used by various tools and generators at CERN.

Detailed documentation can be found `here <https://acc-py.web.cern.ch/gitlab/Cheburashka/PyCheby/docs/stable/>`__.

Quickstart
----------

For more details look at `Quickstart <./docs/source/quickstart.rst>`__.

Examples
--------

For more detailed examples, look at `Examples <./docs/source/examples.rst>`__

Generating Documentation
------------------------

If CERN Documentation server is down or not accessible, it is possible to generate PyCheby API documentation
in a local folder:

.. code::

    git clone --branch <VERSION> https://gitlab.cern.ch/Cheburashka/PyCheby.git
    cd PyCheby
    make docs

or without make, in the project root folder:

.. code::

	sphinx-build -E -v -c docs/source -b html docs/source docs/html

where `docs/html` is the output directory for HTML generator.


Contact & Support
-----------------

cheburashka-support@cern.ch
