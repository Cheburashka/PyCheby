# PyCheby Makefile

PROJECT_NAME = PyCheby
SRC_DIR = PyCheby

PYTHON ?= python3

SPHINX_BUILD ?= sphinx-build
SPHINX_SRC_DIR ?= docs/source
SPHINX_BUILD_DIR ?= docs/html

.PHONY: all
all: package

docs_html:
	$(SPHINX_BUILD) -E -v -c $(SPHINX_SRC_DIR) -b html $(SPHINX_SRC_DIR) $(SPHINX_BUILD_DIR)

docs: docs_html

code_style:
	$(PYTHON) -m pycodestyle $(SRC_DIR)

linter:
	$(PYTHON) -m pylint $(SRC_DIR)

package:
	$(PYTHON) setup.py sdist bdist_wheel

test: clean
	$(PYTHON) -m unittest -v

clean:
	rm -rf dist/
	rm -rf build/
	rm -rf $(PROJECT_NAME).egg-info
	rm -rf $(SPHINX_BUILD_DIR)

