memory-map:
  name: ResamplerBeam2Fixed
  description: Resampler by Javier Galindo
  bus: axi4-lite-32
  size: 4k
  x-gena:
    map-version: 20211022
  x-driver-edge:
    equipment-code: SP-ALCCB
    module-type: RF_RESAMPLER_B2F
  x-map-info:
    ident: 0x00
    memmap-version: 1.0.0
  schema-version:
    core: 2.0.0
    x-conversions: 1.0.0
    x-driver-edge: 1.0.0
    x-enums: 1.0.0
    x-fesa: 2.0.0
    x-gena: 2.0.0
    x-hdl: 1.0.0
    x-map-info: 1.0.0
    x-wbgen: 1.0.0
  x-hdl:
    name-suffix: _regs
    bus-granularity: byte
    bus-attribute: Xilinx
  children:
    - submap:
        name: ipInfo
        address: next
        filename: ipInfo.cheby
        include: true
    - reg:
        name: control
        description: Control register
        width: 32
        access: rw
        address: next
        x-gena:
          rmw: True
        x-fesa:
          multiplexed: False
          persistence: False
        children:
          - field:
              name: toutSelNco
              description: Select reference Tout calculated from NCO ("1") or from the register toutFixed ("0") for resampler
              range: 4
              preset: 0x0
              comment: "Set only once at boot.\ntoutSelNco = 1\n"
    - reg:
        name: toutFixed
        description: Defines the resampling rate in fixed mode (NCO TW/FTW bypassed)
        width: 32
        access: rw
        address: next
        comment: "Normalized ratio of output and input sampling period (inverse of resampling ratio).\nUsed in fixed mode, i.e. to operate at a given RF frequency f_rf regardless of the TW/FTW values coming from the NCO.\nSigned fixed-point format Fix_32_29 (1 sign bit, 2 integer bits and 29 fractional bits).\nGiven the beam synchronous data stream with a given RF frequency f_rf and fixed sampling frequency f_s = 125MHz, this parameter should be set to:\n\n(rsp_fixed2bsp): toutFixed = f_s/f_rf\n\n(rsp_bsp2fixed): toutFixed = f_rf/f_s\n\nWith read/write conversions, the user only needs to provide f_rf.\n\n(rsp_bsp2fixed):\nRange: we can't downsample by more than a factor of 2, and we can represent [-4, 4) with Fix_32_29 format.\n(raw)\nmin-val = 0x00000000 = 0\nmax-val = 0x40000000 = 2\n(tout)\nmin-val = 0\nmax-val = 2\n(f_rf)\nmin-val = f_s*0 = 0\nmax-val = f_s*2 = 250.0e6\n"
        x-fesa:
          multiplexed: true
          persistence: False
          min-val: 0
          max-val: 2.5e+08
        x-conversions:
          read: val*125.0e6/pow(2, 29)
          write: val/125.0e6*pow(2, 29)
    - reg:
        name: numBuckets
        description: Reference number of buckets (SPS harmonic number, h = 4620) for fake bucket data generator and ftw2tout
        width: 32
        access: rw
        address: next
        comment: PPM (different value depending on the cycle type).
        x-fesa:
          multiplexed: False
          persistence: true
    - reg:
        name: gainFifo
        description: Proportional regulation constant for FIFO loop
        width: 32
        access: rw
        address: next
        comment: "(rsp_bsp2fixed): Set only once at boot.\ngainFifo = 1\n"
        x-fesa:
          multiplexed: False
          persistence: true
    - reg:
        name: steadyLevel
        description: FIFO steady level (FIFO loop)
        width: 32
        access: rw
        address: next
        comment: "(rsp_bsp2fixed): Set only once at boot.\nsteadyLevel = 16\n\nRange: 0x00 to 0x1F (only 5 lowest bits of the register used).\n"
        x-fesa:
          multiplexed: False
          persistence: true
          min-val: 0
          max-val: 31
        x-driver-edge:
          min-val: 0x00
          max-val: 0x1F
    - reg:
        name: toutNco
        description: Value of Tout coming from the NCO (non-regulated)
        width: 32
        access: ro
        address: next
        x-fesa:
          multiplexed: False
          persistence: False
        x-conversions:
          read: 125.0e6*val/pow(2, 29)
    - reg:
        name: toutRsp
        description: Value of Tout going to the resampler (regulated)
        width: 32
        access: ro
        address: next
        x-fesa:
          multiplexed: False
          persistence: False
        x-conversions:
          read: 125.0e6*val/pow(2, 29)
    - reg:
        name: fifoLevel
        description: Instantaneous FIFO level (FIFO loop)
        width: 32
        access: ro
        address: next
        x-fesa:
          multiplexed: False
          persistence: false
