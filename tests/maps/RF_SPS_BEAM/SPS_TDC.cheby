memory-map:
  name: TDC
  bus: axi4-lite-32
  size: 4096
  schema-version:
    core: 2.0.0
    x-conversions: 1.0.0
    x-driver-edge: 1.0.0
    x-enums: 1.0.0
    x-fesa: 2.0.0
    x-gena: 2.0.0
    x-hdl: 1.0.0
    x-map-info: 1.0.0
    x-wbgen: 1.0.0
  x-hdl:
    name-suffix: _regs
    bus-granularity: byte
    bus-attribute: Xilinx
  children:
    - reg:
        name: controls
        width: 32
        access: rw
        address: next
        x-fesa:
          multiplexed: True
        children:
          - field:
              name: start
              description: -internal- start delta T measurements
              range: 0
              x-hdl:
                type: autoclear
          - field:
              name: clrAcqLatches
              description: clear acquisition latches (Fc and Frev)
              range: 1
              x-hdl:
                type: autoclear
    - reg:
        name: status
        width: 32
        access: ro
        address: next
        x-fesa:
          multiplexed: True
        children:
          - field:
              name: full
              description: -internal- measurement memory full
              range: 0
          - field:
              name: running
              description: -internal- measurement running
              range: 1
          - field:
              name: s0_dTreset_stoped
              range: 2
              description: -internal- The counter measuring reset to Fc pulse has been started and stoped
          - field:
              name: counter
              description: -internal- number of recorded samples in the delays memory
              range: 15-8
    - reg:
        name: latches
        width: 8
        access: rw
        x-fesa:
          multiplexed: True
        x-hdl:
          type: or-clr
        children:
          - field:
              name: fcPresent
              range: 0
          - field:
              name: frevPresent
              range: 1
    - reg:
        name: s0_dTreset
        description: measured time between reset timing and Fc
        width: 32
        type: unsigned
        access: ro
        address: next
        x-fesa:
          multiplexed: True
    - memory:
        name: delays
        description: -internal- stored delta T measurements
        address: next
        memsize: 1k
        interface: sram
        x-fesa:
          multiplexed: false
        children:
          - reg:
              name: delays
              width: 32
              access: ro