memory-map:
  bus: wb-32-be
  name: clock_monitor
  description: Clock Frequency Monitor
  word-endian: little
  x-wbgen:
    hdl_entity: clock_monitor_wb
  schema-version:
    core: 3.0.0
    x-gena: 2.0.0
    x-hdl: 1.0.0
    x-fesa: 2.0.0
    x-driver-edge: 2.0.0
    x-conversions: 1.0.0
    x-wbgen: 1.0.0
    x-map-info: 1.0.0
    x-enums: 1.0.0
  x-hdl:
    busgroup: true
    name-suffix: _regs
  children:
    - reg:
        name: CR
        address: 0x00000000
        width: 32
        access: rw
        description: Control Register
        children:
          - field:
              name: CNT_RST
              range: 0
              description: Reset counters
              comment: "write 1: resets the counters\nwrite 0: no effect\n"
              x-wbgen:
                type: MONOSTABLE
          - field:
              name: PRESC
              range: 8-1
              description: Prescaler
              x-wbgen:
                type: SLV
                access_bus: READ_WRITE
                access_dev: READ_ONLY
          - field:
              name: REFSEL
              range: 12-9
              description: Reference Select
              x-wbgen:
                type: SLV
                access_bus: READ_WRITE
                access_dev: READ_ONLY
    - reg:
        name: REFDR
        address: 0x00000004
        width: 32
        access: rw
        description: Reference Divider Register
        children:
          - field:
              name: REFDR
              range: 31-0
              description: Reference Division Ratio
              x-wbgen:
                type: SLV
                access_bus: READ_WRITE
                access_dev: READ_ONLY
    - reg:
        name: CNT_SEL
        address: 0x00000008
        width: 32
        access: rw
        description: Counter Select Register
        children:
          - field:
              name: SEL
              range: 3-0
              description: Counter Select
              x-wbgen:
                type: SLV
                access_bus: READ_WRITE
                access_dev: READ_ONLY
    - reg:
        name: CNT_VAL
        address: 0x0000000c
        width: 32
        access: rw
        description: Counter Value Register
        x-hdl:
          write-strobe: true
        children:
          - field:
              name: VALUE
              range: 30-0
              description: Counter Value
              x-wbgen:
                type: SLV
                access_bus: READ_ONLY
                access_dev: WRITE_ONLY
          - field:
              name: VALID
              range: 31
              description: Counter Value Valid/Clear
              x-wbgen:
                type: BIT
                access_bus: READ_WRITE
                access_dev: READ_WRITE
                load: LOAD_EXT
                size: 1
