memory-map:
  name: Application
  description: "Application registers"
  bus: wb-32-be
  x-hdl:
    name-suffix: "Regs"
  schema-version:
    core: 3.0.0
    x-gena: 2.0.0
    x-hdl: 1.0.0
    x-fesa: 2.0.0
    x-driver-edge: 2.0.0
    x-conversions: 1.0.0
    x-wbgen: 1.0.0
    x-map-info: 1.0.0
    x-enums: 1.0.0
  children:
    - reg:
        name: ScalingR1
        description: "Range 1 scaling factor (float)"
        width: 32
        access: rw
    - reg:
        name: OffsetR1
        description: "Range 1 manual offset (float)"
        width: 32
        access: rw
    - reg:
        name: ScalingR3
        description: "Range 3 scaling factor (float)"
        width: 32
        access: rw
    - reg:
        name: OffsetR3
        description: "Range 3 manual offset (float)"
        width: 32
        access: rw
    - reg:
        name: Cmd
        description: "Command register"
        width: 32
        access: rw
        children:
          - field:
              name: SmpTrig
              description: "SMP trigger"
              range: 4
              x-hdl:
                type: autoclear
          - field:
              name: IntIntlkTrig
              description: "Intensity interlock trigger"
              range: 8
              x-hdl:
                type: autoclear
          - field:
              name: SyncAdcs
              description: "Synchronise ADCs"
              range: 16
              x-hdl:
                type: autoclear
          - field:
              name: AcqOn
              description: "Turn acquisition on for all ranges"
              range: 24
              x-hdl:
                type: autoclear
          - field:
              name: AcqOff
              description: "Turn acquisition off for all ranges"
              range: 25
              x-hdl:
                type: autoclear
    - reg:
        name: Ctrl
        description: "Control register"
        width: 32
        access: rw
        children:
          - field:
              name: ManualOffset
              description: "Use manual offset; otherwise automatic"
              range: 0
    - submap:
        name: BisCtrl
        description: "BIS controller registers"
        include: false
        filename: BisController.cheby
    - submap:
        name: IntIntlk
        description: "Intensity interlock registers"
        include: false
        filename: BctInterlock.cheby
    - submap:
        name: Range1
        description: "Range 1 registers"
        address: 0x40000
        include: false
        filename: BctChannel.cheby
    - submap:
        name: Range3
        description: "Range 3 registers"
        address: 0x80000
        include: false
        filename: BctChannel.cheby
    - submap:
        name: Didt
        description: "DIDT registers"
        address: 0x100000
        include: false
        filename: Didt.cheby
    - submap:
        name: BTrain
        description: "BTrain registers"
        include: false
        filename: BTrain.cheby
