memory-map:
  bus: wb-32-be
  name: acq_d3s
  description: D3S Acquisition Buffer
  word-endian: little
  x-hdl:
    busgroup: true
    reg-prefix: true
  x-wbgen:
    hdl_entity: acq_d3s_ctrl_wb
  schema-version:
    core: 2.0.0
    x-gena: 2.0.0
    x-hdl: 1.0.0
    x-fesa: 2.0.0
    x-driver-edge: 1.0.0
    x-conversions: 1.0.0
    x-wbgen: 1.0.0
    x-map-info: 1.0.0
    x-enums: 1.0.0
  children:
    - reg:
        name: csr
        address: 0x00000000
        width: 32
        access: rw
        description: Control/Status Reg
        children:
          - field:
              name: start
              range: 0
              description: Start sampling
              comment: "write 1: acquisition buffer will immediately start sampling incoming date\nwrite 0: no effect\n"
              x-wbgen:
                type: MONOSTABLE
                clock: clk_acq_i
          - field:
              name: ready
              range: 1
              description: Ready
              comment: "read 1: acquisition buffer has completed the acquisition (filled the whole buffer)\nread 0: acquisition in progress\n"
              x-wbgen:
                type: BIT
                access_bus: READ_ONLY
                access_dev: WRITE_ONLY
                clock: clk_acq_i
          - field:
              name: mode
              range: 2
              description: Circular buffer mode
              comment: "0: one-shot acquisition mode: start at first word in the buffer and keep recording until the end of the buffer\n1: circular buffer mode: keep sampling continuously, writing 1 to CR.FREEZE halts the acquisition, last acquired sample is at POINTER address in the buffer\n"
              x-wbgen:
                type: BIT
                access_bus: READ_WRITE
                access_dev: READ_ONLY
                clock: clk_acq_i
          - field:
              name: freeze
              range: 3
              description: Stop circular acquisition
              comment: "write 1: halts acquisition when in circular buffer mode (cr.mode = 1). In single-shot mode, no action is taken.\nwrite 0: no effect\n"
              x-wbgen:
                type: BIT
                access_bus: READ_WRITE
                access_dev: READ_ONLY
                clock: clk_acq_i
    - reg:
        name: pointer
        address: 0x00000004
        width: 32
        access: ro
        description: Circular buffer pointer
        comment: "Address in the buffer (index of sample) of the last acquired sample before freezing acquisition in circular buffer mode.\n"
        x-wbgen:
          type: SLV
          access_bus: READ_ONLY
          access_dev: WRITE_ONLY
          clock: clk_acq_i
          field_description: Address
    - reg:
        name: address
        address: 0x00000008
        width: 32
        access: rw
        description: Buffer address
        comment: "Address in the buffer (index of sample) the data register will point to.\n"
        x-wbgen:
          type: SLV
          access_bus: READ_WRITE
          access_dev: READ_ONLY
          field_description: Address
    - reg:
        name: data
        address: 0x0000000c
        width: 32
        access: ro
        description: Buffer data
        comment: "Sample value at address given by address register in the buffer.\n"
        x-wbgen:
          type: SLV
          access_bus: READ_ONLY
          access_dev: WRITE_ONLY
          field_description: Data