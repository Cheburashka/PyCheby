memory-map:
  bus: wb-32-be
  name: wr_rf
  description: WR RF transmission control
  comment: "-----------------------------------------------------------------\nWishbone registers to manage/debug transmission and reception of\nRF frames using WR Streamers\n-----------------------------------------------------------------\nCopyright (c) 2016 CERN/BE-CO-HT and CERN/TE-MS-MM\n\nThis source file is free software; you can redistribute it\nand/or modify it under the terms of the GNU Lesser General\nPublic License as published by the Free Software Foundation;\neither version 2.1 of the License, or (at your option) any\nlater version.\n\nThis source is distributed in the hope that it will be\nuseful, but WITHOUT ANY WARRANTY; without even the implied\nwarranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR\nPURPOSE.  See the GNU Lesser General Public License for more\ndetails\n\nYou should have received a copy of the GNU Lesser General\nPublic License along with this source; if not, download it\nfrom http://www.gnu.org/licenses/lgpl-2.1.html\n-----------------------------------------------------------------\n"
  word-endian: little
  x-wbgen:
    hdl_entity: RF_wb
    version: 1
  schema-version:
    core: 3.0.0
    x-gena: 2.0.0
    x-hdl: 1.0.0
    x-fesa: 2.0.0
    x-driver-edge: 2.0.0
    x-conversions: 1.0.0
    x-wbgen: 1.0.0
    x-map-info: 1.0.0
    x-enums: 1.0.0
  x-hdl:
    busgroup: true
    name-suffix: _regs
  children:
    - reg:
        name: VER
        address: 0x00000000
        width: 32
        access: rw
        description: Version register
        children:
          - field:
              name: ID
              range: 31-0
              description: Version identifier
              preset: 1
              x-wbgen:
                type: SLV
                access_bus: READ_WRITE
                access_dev: READ_ONLY
    - reg:
        name: SCR
        address: 0x00000004
        width: 32
        access: rw
        description: Status and ctontrol register
        children:
          - field:
              name: TX_SINGLE
              range: 0
              description: Transmit single frame
              comment: "Transmit single WR RF frame\n"
              x-wbgen:
                type: MONOSTABLE
          - field:
              name: TX_DBG
              range: 2-1
              description: Debug mode
              comment: "Debugging mode:\n0: debugging disabled\n1: send values of incremented counters in the frames\n2: use for RF value provided by the RFvalueSimGen, programmed separetaly\n3: send frames with dummy content (see RF FrameTransceiver_pkg)\n"
              x-wbgen:
                type: SLV
                access_bus: READ_WRITE
                access_dev: READ_ONLY
          - field:
              name: TX_DBG_FTYPE
              range: 7-4
              description: Debug Frame type
              comment: "Debugging of forced frame type:\n0: do not enforce (send the type that is input to module)\n1: force RFm frames\n2: force RFs frames\n3: force PFW frames\n"
              x-wbgen:
                type: SLV
                access_bus: READ_WRITE
                access_dev: READ_ONLY
          - field:
              name: RX_VALID_POL_INV
              range: 8
              description: Invert RX valid polarity
              comment: "0: normal polarity (active high)\n1: inverted polarity (active low)\n"
              x-wbgen:
                type: BIT
                access_bus: READ_WRITE
                access_dev: READ_ONLY
          - field:
              name: TX_OR_CONFIG
              range: 9
              description: TX override default config
              comment: "0: default configuration of the Tx period (taken from generics)\n1: values of the Tx period from WB registers\n"
              x-wbgen:
                type: BIT
                access_bus: READ_WRITE
                access_dev: READ_ONLY
          - field:
              name: RX_OR_CONFIG
              range: 10
              description: RX override default config
              comment: "0: default configuration of the Rx valid period/delay and polarity (taken from generics)\n1: values of the Rx valid period/delay and polarity from WB registers\n"
              x-wbgen:
                type: BIT
                access_bus: READ_WRITE
                access_dev: READ_ONLY
          - field:
              name: DUMMY
              range: 31-16
              description: Dummy
              comment: "dumy\n"
              x-wbgen:
                type: SLV
                access_bus: READ_ONLY
                access_dev: WRITE_ONLY
    - reg:
        name: TX_PERIOD
        address: 0x00000008
        width: 32
        access: rw
        description: Transmission period
        children:
          - field:
              name: value
              range: 31-0
              description: "Period in clock cylces (16ns). When the value is not zero, frames are sent with that period. "
              preset: 0
              x-wbgen:
                type: SLV
                access_bus: READ_WRITE
                access_dev: READ_ONLY
    - reg:
        name: RX_OUT_DATA_TIME
        address: 0x0000000c
        width: 32
        access: rw
        description: Rx output data valid delay and period
        comment: "Defines the time for which the received data is valid (in clk cycles).\n"
        children:
          - field:
              name: valid
              range: 15-0
              description: Output data valid period
              comment: "Period in clock cylces (16ns) during which data is valid. By default it is 1 cycle strobe. Two special values\n0x0000: output data disabled\n0xFFFF: output data continuously valid until next update (might be invalid for the time defined by delay).\n"
              preset: 1
              x-wbgen:
                type: SLV
                access_bus: READ_WRITE
                access_dev: READ_ONLY
          - field:
              name: delay
              range: 31-16
              description: Output data delay
              comment: "Delay in clock cylces (16ns) between receiving data and making it valid. By default it is 0 cycle\n"
              preset: 0
              x-wbgen:
                type: SLV
                access_bus: READ_WRITE
                access_dev: READ_ONLY
    - reg:
        name: SIMRF_CTRL
        address: 0x00000010
        width: 32
        access: rw
        description: "SimRFval: control subcycles"
        children:
          - field:
              name: CLEN
              range: 3-0
              description: Cycle Length
              comment: "Cycle lenght counted in the number of sub-cycles that compose a full cycle. The full cycle is repeated indefinitely, special values:\n0x0: generation disabled\n0xF: generation continous (uses increment from sub-cycle 0 indefinitely)\n"
              x-wbgen:
                type: SLV
                access_bus: READ_WRITE
                access_dev: READ_ONLY
          - field:
              name: SCID
              range: 7-4
              description: Sub-cycle ID
              comment: "The ID (number) of the sub-cycle for which parameters can be set through registers BSIM_CYC_LEN and BSIM_CYC_INC\n"
              x-wbgen:
                type: SLV
                access_bus: READ_WRITE
                access_dev: READ_ONLY
          - field:
              name: CMAXLEN
              range: 11-8
              description: Cycle maximum length
              comment: "The maximum number of sub-cycles that can be configured.\n"
              x-wbgen:
                type: SLV
                access_bus: READ_ONLY
                access_dev: WRITE_ONLY
    - reg:
        name: RFSIM_SCYC_LEN
        address: 0x00000014
        width: 32
        access: rw
        description: "SimRFval: Sub-cycle lenght"
        comment: "Lenght of the sub-cycle with SCID\n"
        x-hdl:
          write-strobe: true
        children:
          - field:
              name: val
              range: 31-0
              description: "Lenght in sent frames, i.e. the "
              x-wbgen:
                type: SLV
                access_bus: READ_WRITE
                access_dev: READ_WRITE
                load: LOAD_EXT
    - reg:
        name: RFSIM_SCYC_INC
        address: 0x00000018
        width: 32
        access: rw
        description: "SimRFval: Sub-cycle increment"
        comment: "Increment of RFvalue for the sub-cycle with SCID\n"
        x-hdl:
          write-strobe: true
        children:
          - field:
              name: val
              range: 15-0
              description: Signed value that is added to the RFvalue each time RF frame is transmitted.
              x-wbgen:
                type: SLV
                access_bus: READ_WRITE
                access_dev: READ_WRITE
                load: LOAD_EXT
    - reg:
        name: DBG_RX_RFm_or_RFs
        address: 0x0000001c
        width: 32
        access: ro
        description: DBG RX RFm or RFs value
        comment: "It provides access to the most recently received value of RFm or RFs, depending on the RF frame type\n"
        x-wbgen:
          type: SLV
          access_bus: READ_ONLY
          access_dev: WRITE_ONLY
          field_description: Debug content
    - reg:
        name: DBG_TX_RFm_or_RFs
        address: 0x00000020
        width: 32
        access: ro
        description: DBG TX RFm or RFs value, depending which frame
        comment: "It provides access to the most recently transmitted value of RFm or RFs, depending on the RF frame type\n"
        x-wbgen:
          type: SLV
          access_bus: READ_ONLY
          access_dev: WRITE_ONLY
          field_description: Debug content
