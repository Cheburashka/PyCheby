memory-map:
  name: polarLoop
  bus: axi4-lite-32
  size: 4096
  x-map-info:
    memmap-version: 1.1.1
  schema-version:
    core: 3.0.0
    x-conversions: 1.0.0
    x-driver-edge: 2.0.0
    x-enums: 1.0.0
    x-fesa: 2.0.0
    x-gena: 2.0.0
    x-hdl: 1.0.0
    x-map-info: 1.0.0
    x-wbgen: 1.0.0
  x-hdl:
    name-suffix: _regs
    bus-attribute: Xilinx
    bus-granularity: byte
  children:
    - reg:
        name: aSetMin
        description: Setpoint enable threshold (fullscale = 2*vNom)
        width: 16
        access: rw
        address: next
        type: unsigned
        x-fesa:
          multiplexed: false
          persistence: true
        x-conversions:
          read: val/pow(2.0, 16.0)
          write: val*pow(2.0, 16.0)
    - reg:
        name: aIcFwdMin
        description: IcFwd enable threshold, raw = sqrt(aIcFwdMin/(4*pNom))
        width: 16
        access: rw
        address: next
        type: unsigned
        x-fesa:
          multiplexed: false
          persistence: true
        x-conversions:
          read: val/pow(2.0, 16.0)
          write: val*pow(2.0, 16.0)
    - reg:
        name: phaseOSCMax
        description: Phase lock tolerance
        width: 16
        access: rw
        address: next
        type: signed
        x-fesa:
          multiplexed: false
          persistence: true
        x-conversions:
          read: val/pow(2.0,15.0)*180.0
          write: val*pow(2.0,15.0)/180.0
        x-driver-edge:
          min-val: 0
          max-val: 32767
    - reg:
        name: gainOSCMax
        description: Gain lock tolerance
        width: 16
        access: rw
        address: next
        type: signed
        x-fesa:
          multiplexed: false
          persistence: true
        x-conversions:
          read: val/pow(2.0, 14.0)
          write: val*pow(2.0, 14.0)
        x-driver-edge:
          min-val: 0
          max-val: 32767
    - reg:
        name: gainLoopGain
        description: Gain for gain loop
        width: 16
        access: rw
        address: next
        type: signed
        x-fesa:
          multiplexed: false
          persistence: true
        x-conversions:
          read: val/pow(2.0, 14.0)
          write: val*pow(2.0, 14.0)
        x-driver-edge:
          min-val: 0
          max-val: 32767
    - reg:
        name: phaseLoopGain
        description: Reserved
        width: 16
        access: rw
        address: next
        type: signed
        x-fesa:
          multiplexed: false
          persistence: true
        x-conversions:
          read: val/pow(2.0, 14.0)
          write: val*pow(2.0, 14.0)
        x-driver-edge:
          min-val: 0
          max-val: 32767
    - reg:
        name: phaseOffset
        description: Phase Offset Value
        width: 16
        access: rw
        address: next
        type: signed
        x-fesa:
          multiplexed: false
          persistence: true
        x-conversions:
          read: val/pow(2.0,15.0)*180.0
          write: val*pow(2.0,15.0)/180.0
    - reg:
        name: gainSetpoint
        description: Gain Setpoint Value
        width: 16
        access: rw
        address: next
        type: unsigned
        x-fesa:
          multiplexed: false
          persistence: true
          min-val: 0
          max-val: 2
        x-conversions:
          read: val/pow(2.0, 14.0)
          write: val*pow(2.0, 14.0)
        x-driver-edge:
          min-val: 0
          max-val: 32767
    - reg:
        name: polarLoopStatus
        description: Polar Loop Status Registers
        width: 16
        access: ro
        address: next
        x-fesa:
          multiplexed: false
          persistence: true
        children:
          - field:
              name: gainLocked
              range: 1
          - field:
              name: phaseLocked
              range: 2
          - field:
              name: rfLow
              range: 3
              description: Show if input signal are below the threshold (setpoint or Cavity forward)
          - field:
              name: gainLoopStop
              range: 4
              description: Indicates if the loop is active or stop. Can stop if rfLow or from external input
          - field:
              name: phaseLoopStop
              range: 5
              description: Indicates if the loop is active or stop. Can stop if rfLow or from external input
    - reg:
        name: enables
        access: rw
        width: 8
        children:
          - field:
              name: polarLoopEnable
              range: 0
