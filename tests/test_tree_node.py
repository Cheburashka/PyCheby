# PyCheby
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

import unittest

import tests.config as config

from PyCheby.PyCheby import PyCheby
from tests.config import MEMMAP_PATH

class TreeNodeTestCase(unittest.TestCase):
    '''
    Tree Node general purpose tests, being the base class for all Memory Nodes.
    '''
    def setUp(self):
        self.memmap_path = MEMMAP_PATH['RF_SPS200_LOOPS_AS']
        self.tree = PyCheby(self.memmap_path, ignore_version_check=True)
        self.topmap = self.tree.get()
        self.bar0 = self.topmap.children['bar0']
        self.bar4 = self.topmap.children['bar4']

    def test_node_names(self):
        # select random submap and register
        submap = self.bar0.children['app'].children['fgc']
        reg = self.bar0.children['app'].children['rfnco'].children['ipInfo'].children['ident']

        self.assertEqual(submap.name, 'fgc')
        self.assertEqual(submap.get_full_name(), 'app_fgc')
        self.assertEqual(submap.get_full_name(separator='.'), 'app.fgc')

        self.assertEqual(reg.name, 'ident')
        self.assertEqual(reg.get_full_name(), 'app_rfnco_ipInfo_ident')

    def test_node_path(self):
        # select random submap and register
        submap = self.bar0.children['app'].children['fgc']
        reg = self.bar0.children['app'].children['excitation'].children['acqCore'].children['ipInfo'].children['ident']

        self.assertEqual(submap.get_full_path(), 'sps200CavityControl/bar0/app/fgc')
        self.assertEqual(submap.get_full_path(separator='.'), 'sps200CavityControl.bar0.app.fgc')

        self.assertEqual(reg.get_full_path(), 'sps200CavityControl/bar0/app/excitation/acqCore/ipInfo/ident')

    def test_node_rel_name_list(self):
        '''
        Check whether relative names list are correctly parsed.

        Address-Spaces should be fully transparent to relative names and should not be in the list.
        The relative names list is used e.g. to generate EDGE driver files.

        Expected values based on legacy version v0.0.3.
        '''
        # select random submap and register
        submap = self.bar0.children['app'].children['fgc']
        reg = self.bar0.children['app'].children['excitation'].children['acqCore'].children['ipInfo'].children['ident']

        # Different list depends on the parent
        self.assertEqual(submap.rel_name_list(self.topmap), ['app', 'fgc'])
        self.assertEqual(submap.rel_name_list(submap.parent), ['fgc'])
        self.assertEqual(submap.rel_name_list(reg), ['app', 'fgc'])

        # Register
        self.assertEqual(reg.rel_name_list(self.topmap), ['app', 'excitation', 'acqCore', 'ipInfo', 'ident'])
        self.assertEqual(reg.rel_name_list(reg.parent), ['ident'])
        self.assertEqual(reg.rel_name_list(submap), ['app', 'excitation', 'acqCore', 'ipInfo', 'ident'])

    def test_node_parents(self):
        # select random submap and register
        submap = self.bar0.children['hwInfo']
        reg = self.bar0.children['app'].children['rfnco'].children['ipInfo'].children['ident']

        reg_parents = set([p.name for p in reg.parents])
        submap_parents = set([p.name for p in submap.parents])

        expected_reg_parents = set(['sps200CavityControl', 'bar0', 'app', 'rfnco', 'ipInfo'])
        expected_submap_parents = set(['sps200CavityControl', 'bar0'])

        self.assertEqual(reg_parents, expected_reg_parents)
        self.assertEqual(submap_parents, expected_submap_parents)


if __name__ == '__main__':
    unittest.main()
