# PyCheby
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

import unittest

from PyCheby.PyCheby import PyCheby

from tests.config import MEMMAP_PATH
from tests.config import MEMMAP_DIR

class MapLoaderTestCase(unittest.TestCase):
    def test_empty_path(self):
        with self.assertRaises(FileNotFoundError):
            PyCheby('', ignore_version_check=True)

    def test_file_invalid_extension(self):
        with self.assertRaises(KeyError):
            PyCheby(f'{MEMMAP_DIR}/File.invalid_extension', ignore_version_check=True)

    def test_file_not_exists(self):
        with self.assertRaises(FileNotFoundError):
            PyCheby(f'{MEMMAP_DIR}/This/Path/Do/Not/Exists/File.cheby', ignore_version_check=True)

    def test_rf_sps_beam(self):
        map_path = MEMMAP_PATH['RF_SPS_BEAM']
        obj = PyCheby(map_path, ignore_version_check=True)
        topmap = obj.get()
        submaps = set(topmap.children.keys())

        expected_submaps = set({
            'acqCoreExt_ddr',
            'hwInfo',
            'acqCoreInt_ram3',
            'fgc1_ddr',
            'app',
            'fgc3_ddr',
            'fgc4_ddr',
            'fgc2_ddr',
            'afcz'
        })

        self.assertEqual(obj.filename,  'AFCZ-BeamControl.cheby')
        self.assertEqual(topmap.c_size, 0x140000)                   # BAR0 size only
        self.assertEqual(submaps,       expected_submaps)

    def test_rf_sps200_loops(self):
        map_path = MEMMAP_PATH['RF_SPS200_LOOPS']
        obj = PyCheby(map_path, ignore_version_check=True)
        topmap = obj.get()
        submaps = set(topmap.children.keys())

        expected_submaps = set({
            'hwInfo',
            'sis8300ku',
            'app',
            'fgc_ddr',
            'acq_ddr',
            'acq_ram',
            'acq_ram2',
            'acq_ram3',
            'acq_exc'
        })

        self.assertEqual(obj.filename,  'sps200CavityControl.cheby')
        self.assertEqual(topmap.c_size, 0x200000)                   # BAR0 size only
        self.assertEqual(submaps,       expected_submaps)

    def test_check_version_schema(self):
        '''
        Check whether minimal version schema checks fail for older maps.
        '''

        with self.assertRaises(Exception):
            map_path = MEMMAP_PATH['RF_SPS200_LOOPS']
            obj = PyCheby(map_path, ignore_version_check=False)

