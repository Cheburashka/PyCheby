# PyCheby
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

import unittest

import tests.config as config

from PyCheby.PyCheby import PyCheby
from PyCheby.memory_nodes import InterruptControllerType

from tests.config import MEMMAP_PATH
from tests.testbase import RFTestCaseBase

class BCTDCDIDTSPSTestCase(RFTestCaseBase):
    '''
    BCTDC_DIDT_SPS Test Case for a VME card with Address-Spaces
    '''
    def setUp(self):
        self.expected = RFTestCaseBase.load_expected_results('BCTDC_DIDT_SPS')

        # Only one Address-Space expected: "Registers"
        self.expected_registers, *_ = [child['Registers'] for child in self.expected['children'] if 'Registers' in child.keys()]

        # One of two main submaps of the "Registers" Address-Space
        self.expected_application, *_ = [child['Application'] for child in self.expected_registers['children'] if 'Application' in child.keys()]

        self.memmap_path = MEMMAP_PATH['BCTDC_DIDT_SPS']
        self.tree = PyCheby(self.memmap_path, ignore_version_check=True)

        self.topmap = self.tree.get()
        self.registers = self.topmap.children['Registers'] # Address-Space
        self.application = self.registers.children['Application'] # One of two main submaps of the Registers

    def test_attrs(self):
        '''
        Check top memory map attributes
        '''
        self.assertEqual(self.tree.filename, self.expected['filename'])
        self.assertEqual(self.topmap.bus, self.expected['bus'])

    def test_attrs_size(self):
        '''
        Check size attributes
        '''
        self.assertEqual(self.topmap.c_size, self.expected['size'])
        self.assertEqual(self.registers.c_size, self.expected_registers['size'])

    def test_attrs_x_drv_edge(self):
        '''
        Check x_drv_edge attributes for the top map
        '''
        expected = self.expected['x-driver-edge']
        self.assertEqual(self.topmap.driver_edge.driver_version, expected['driver-version'])
        self.assertEqual(self.topmap.driver_edge.schema_version, expected['schema-version'])
        self.assertEqual(self.topmap.driver_edge.bus_type, expected['bus-type'])
        self.assertEqual(self.topmap.driver_edge.endianness, expected['endianness'])

    def test_children(self):
        self.check_children(self.topmap, self.expected['children'])

        self.check_children(self.registers, self.expected_registers['children'])
        self.check_children(self.registers, self.expected_registers['children'], recursive=False, strict=True)

        self.check_children(self.application, self.expected_application['children'])
        self.check_children(self.application, self.expected_application['children'], recursive=False, strict=True)

    def test_interrupt_controllers(self):
        '''
        Check Interrupt Controller registers (EDGE3-only)
        '''
        # Expected precisely one registers containing a Interrupt Controller
        reg, *_ = [node for node in self.topmap.walk() if node.is_register() and node.has_interrupt_controllers()]
        expected = self.expected['interrupt-controller']
        self.assertEqual(reg.name, expected['reg_name'])

        # Expected precisely only one Interrupt Controller in the register
        intc, *_ = [child for child in reg.interrupt_controllers]

        self.assertTrue(isinstance(intc.type, InterruptControllerType))
        self.assertEqual(intc.name, expected['name'])
        self.assertEqual(intc.type.value, expected['type']) # expected stored as a string

if __name__ == '__main__':
    unittest.main()
