# PyCheby
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

import unittest

import tests.config as config

from PyCheby.PyCheby import PyCheby
from tests.config import MEMMAP_PATH
from tests.testbase import RFTestCaseBase

class RFLN4CAVLPV3(RFTestCaseBase):
    '''
    RF_LN4_CAVLPV3 Test Case
    '''
    def setUp(self):
        self.expected = RFTestCaseBase.load_expected_results('RF_LN4_CAVLPV3')

        self.memmap_path = MEMMAP_PATH['RF_LN4_CAVLPV3']
        self.tree = PyCheby(self.memmap_path, ignore_version_check=True)

        self.topmap = self.tree.get()

    def test_attrs(self):
        '''
        Check top memory map attributes
        '''
        self.assertEqual(self.tree.filename, self.expected['filename'])
        self.assertEqual(self.topmap.bus, self.expected['bus'])
        self.assertEqual(self.topmap.c_size, self.expected['size'])

        # Check x-gena attributes for the top map
        self.assertEqual(self.topmap.gena.map_version, self.expected['x-gena']['map-version'])

        # Check x-map-info attributes for the top map
        self.assertEqual(self.topmap.map_info.ident, self.expected['x-map-info']['ident'])
        self.assertEqual(self.topmap.map_info.memmap_version, self.expected['x-map-info']['memmap-version'])

    def test_attrs_x_drv_edge(self):
        '''
        Check x-drv-edge attributes for the top map
        '''
        expected = self.expected['x-driver-edge']
        self.assertEqual(self.topmap.driver_edge.equipment_code, expected['equipment-code'])
        self.assertEqual(self.topmap.driver_edge.module_type, expected['module-type'])

    def test_x_fesa_configuration_values(self):
        '''
        Check Configuration Values for FESA generator
        '''
        expected = self.expected['configuration-values']

        for cfgval in self.topmap.configuration_values:
            self.assertEqual(expected[cfgval.name]['name'], cfgval.name)
            self.assertEqual(expected[cfgval.name]['value'], cfgval.value)
            self.assertEqual(expected[cfgval.name]['description'], cfgval.description)
            self.assertEqual(expected[cfgval.name]['full_name'], cfgval.full_name)

if __name__ == '__main__':
    unittest.main()
