# PyCheby
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

import unittest
import ruamel.yaml as yaml
import tests.config as config

from PyCheby.PyCheby import PyCheby
from PyCheby.memory_nodes import MemoryNode

from tests.config import MEMMAP_PATH
from tests.config import RESULTS_FILE_PATH

class RFTestCaseBase(unittest.TestCase):
    def check_attributes(self, node, expected: dict, excluded: list = None):
        '''
        Check PyCheby node's attributes.

        PyCheby stores attribute in the node's attributes or in the node's attribute extensions.
        PyCheby attribute extensions (selected):
        - 'driver_edge' (in the map as 'x-driver-edge')
        - 'gena' (in the map as 'x-gena')
        - 'fesa' (in the map as 'x-fesa')
        '''
        def _err_msg(node, attr_name, attr_val, expected) -> str:
            '''
            Error message helper for unittest package
            '''
            node_name = node.get_full_path() if isinstance(node, MemoryNode) else node.parent.get_full_path()
            return f'check_attributes: node "{node_name}": attribute "{attr_name}"(={attr_val}) != expected(={expected})'

        if not expected:
            return True

        if node is None:
            raise Exception(f'check_attributes: node cannot be None for the expected attributes!')

        for attr_name, expected_val in expected.items():
            if excluded and attr_name in excluded:
                continue

            # Attribute container starts with 'x-' prefix
            if attr_name.startswith('x-'):
                container_name = attr_name.replace('x-', '').replace('-', '_')
                attrs_container = node.get(container_name)
                self.check_attributes(attrs_container, expected_val)
                continue

            is_dict = isinstance(expected_val, dict)

            # Check if node contains the attribute container
            if is_dict and not node.get(attr_name):
                raise Exception(f'check_attributes: node "{node.parent.get_full_path()}": expected attribute container "{attr_name}" not exists!')

            # Attribute container exists in the node, recursive call to check attribute by attribute
            if is_dict:
                self.check_attributes(node, expected_val)
                continue

            # Get attribute value from the node or from a node's extension
            is_extension = isinstance(node, MemoryNode.Extension)
            val = node.get(attr_name) if not is_extension else node.extension.get(attr_name)

            # Attribute can be in another nested dictiorary
            if val is None:
                val, *_ = [attr.get(attr_name) for attr in node.extension.values()]

            self.assertEqual(val, expected_val, msg=_err_msg(node, attr_name, val, expected_val))


    def check_children(self, node:MemoryNode, expected_children:list, recursive:bool = True, strict:bool = False) -> bool:
        '''
        Check if node contains all required children (submaps, Address-Spaces, registers, fields etc)
        and children attributes (if defined) are correct.

        E.g. for a YAML file:
        """
        topmap:
            children:
                - hwInfo
                - sis8300ku:
                    address: 0x80000
                    children:
                        - ipInfo:
                            address: 0x80010
                        - lm32
                - app:
                    children:
                        - cavityLoops
                        - modulation
                        - timingGen
                - fgc_ddr
                - acq_ddr
        """
        expected children (topmap)      = [hwInfo, {"sis8300ku": {...}}, {"app": {...}, fgc_ddr, acq_ddr]
        expected children (sis8300ku)   = [ipInfo, lm32]
        expected children (app)         = [cavityLoops, modulation, timingGen]

        expected attributes (sis8300ku) = {'address': 0x80000, "children": [{"ipInfo": {...}}, lm32]}
        expected attributes (ipInfo)    = {'address': 0x80010}
        '''
        def _err_msg(node:MemoryNode, msg:str) -> str:
            return f'check_children: node "{node.get_full_path()}": {msg}'

        if not expected_children:
            return True

        # extract expected children names from YAML dict, some of them are a string, some dictionary with attributes
        expected_names = set()
        for child in expected_children:
            if not isinstance(child, dict):
                expected_names.add(child) # single child without its own children or attributes
                continue

            # extract child parameters from the current child
            child_name, *_ = child.keys()
            child_attrs, *_ = child.values()
            expected_names.add(child_name)

            if not recursive:
                continue

            # find node corresponding to the current child name, and check its children (if defined)
            child_node, *_ = [n for n in node.children.values() if n.name == child_name]
            self.check_children(child_node, child_attrs.get('children', None))

            # check child-related attributes if defined, excluding children
            self.check_attributes(node=child_node, expected=child_attrs, excluded=['children'])

        # Check if expected children names are a subset of the node children
        node_children_names = set(node.children.keys())
        if not strict:
            self.assertTrue(node_children_names.issuperset(expected_names),
                            msg=_err_msg(node, f'expected children are not a subset of node children: missing "{expected_names - node_children_names}"'))
            return True

        # strict check - children names extracted from input YAML dict must match all node children
        self.assertEqual(node_children_names, expected_names, msg=_err_msg(node, 'expected children dont match node children (strict check)'))

    @staticmethod
    def load_expected_results(map_name: str, yamlpath: str=RESULTS_FILE_PATH) -> dict:
        valid_map_names = MEMMAP_PATH.keys()
        if not map_name in valid_map_names:
            raise KeyError(f'Memory map "{map_name}" not supported. Accepted maps = [{valid_map_names}]')

        with open(yamlpath, 'r') as f:
            data = yaml.YAML(typ='safe').load(f)
            return data[map_name]
