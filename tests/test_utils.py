# PyCheby
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

import unittest
import PyCheby.utils as utils

class UtilsTestCase(unittest.TestCase):
    def test_str_to_int(self):
        self.assertEqual(utils.str_to_int('0'), 0)
        self.assertEqual(utils.str_to_int('0001234'), 1234)

        self.assertEqual(utils.str_to_int('0x2137'), 0x2137)
        self.assertEqual(utils.str_to_int('-0x2137'), -0x2137)

        # the same with empty spaces
        self.assertEqual(utils.str_to_int('   0x2137'), 0x2137)
        self.assertEqual(utils.str_to_int('0x2137   '), 0x2137)

        self.assertEqual(utils.str_to_int('2137'), 2137)
        self.assertEqual(utils.str_to_int('-2137'), -2137)

        # return the input value itself, if its type is int
        self.assertEqual(utils.str_to_int(-2137), -2137)
        self.assertEqual(utils.str_to_int(2137), 2137)
        self.assertEqual(utils.str_to_int(2137.0), 2137)
        self.assertEqual(utils.str_to_int(-2137.0), -2137)

        with self.assertRaises(ValueError):
            utils.str_to_int('')

        with self.assertRaises(ValueError):
            utils.str_to_int('-0x21x37')

        with self.assertRaises(ValueError):
            utils.str_to_int('0x 2137')

if __name__ == '__main__':
    unittest.main()
