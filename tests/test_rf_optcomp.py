# PyCheby
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

import unittest

import tests.config as config

from PyCheby.PyCheby import PyCheby
from tests.config import MEMMAP_PATH
from tests.testbase import RFTestCaseBase

class RFOptCompTestCase(RFTestCaseBase):
    '''
    RF_OPT_COMP Test Case for a standard VME card
    '''
    def setUp(self):
        self.expected = RFTestCaseBase.load_expected_results('RF_OPTCOMP')

        # One of blocks: "psMon"
        self.expected_psmon, *_ = [child['psMon'] for child in self.expected['children'] if isinstance(child, dict) and 'psMon' in child.keys()]

        self.memmap_path = MEMMAP_PATH['RF_OPTCOMP']
        self.tree = PyCheby(self.memmap_path, ignore_version_check=True)

        self.topmap = self.tree.get()
        self.psmon = self.topmap.children['psMon']

    def test_attrs(self):
        '''
        Check top memory map attributes
        '''
        self.assertEqual(self.tree.filename, self.expected['filename'])
        self.assertEqual(self.topmap.bus, self.expected['bus'])
        self.assertEqual(self.topmap.c_size, self.expected['size'])

        # Check x-gena attributes for the top map
        self.assertEqual(self.topmap.gena.map_version, self.expected['x-gena']['map-version'])

        # Check x-map-info attributes for the top map
        self.assertEqual(self.topmap.map_info.ident, self.expected['x-map-info']['ident'])

    def test_attrs_x_drv_edge(self):
        '''
        Check x-drv-edge attributes for the top map
        '''
        expected = self.expected['x-driver-edge']
        self.assertEqual(self.topmap.driver_edge.equipment_code, expected['equipment-code'])
        self.assertEqual(self.topmap.driver_edge.module_type, expected['module-type'])

    def test_children(self):
        self.check_children(self.topmap, self.expected['children'])

        self.check_children(self.psmon, self.expected_psmon['children'])
        self.check_children(self.psmon, self.expected_psmon['children'], recursive=False, strict=True)

if __name__ == '__main__':
    unittest.main()
