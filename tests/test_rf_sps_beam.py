# PyCheby
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

import unittest

from PyCheby.PyCheby import PyCheby

from tests.config import MEMMAP_PATH
from tests.testbase import RFTestCaseBase

class RFSPSBeamTestCase(RFTestCaseBase):
    def setUp(self):
        self.expected = RFTestCaseBase.load_expected_results('RF_SPS_BEAM')
        self.expected_bar0 = self.expected['address-spaces']['bar0']

        # One of main submaps: "afcz"
        self.expected_afcz, *_ = [child['afcz'] for child in self.expected['children'] if 'afcz' in child.keys()]

        self.memmap_path = MEMMAP_PATH['RF_SPS_BEAM']
        self.tree = PyCheby(self.memmap_path, ignore_version_check=True)

        self.topmap = self.tree.get()
        self.afcz = self.topmap.children['afcz']

    def test_attrs(self):
        '''
        Check top memory map attributes

        IMPORTANT NOTE:
        The top memory size is associated with ONLY the BAR0 address space for all maps,
        that have not been migrated to Address-Spaces (legacy).

        From PyCheby v1.1.0 supports Address-Spaces, if a map has already been migrated to Address-Spaces,
        the top size is always associated with the entire address space.
        '''
        self.assertEqual(self.tree.filename, self.expected['filename'])
        self.assertEqual(self.topmap.bus, self.expected['bus'])

        # BAR0 size only for non-Address-Space map
        self.assertEqual(self.topmap.c_size, self.expected_bar0['size']) # BAR0 size only

    def test_attrs_x_drv_edge(self):
        '''
        Check x_drv_edge attributes for the top map
        '''
        expected = self.expected['x-driver-edge']
        self.assertEqual(self.topmap.driver_edge.equipment_code, expected['equipment-code'])
        self.assertEqual(self.topmap.driver_edge.module_type, expected['module-type'])
        self.assertEqual(self.topmap.driver_edge.bus_type, expected['bus-type'])

    def test_attrs_x_fesa(self):
        '''
        Check x-fesa attributes for the top map
        '''
        expected = self.expected['x-fesa']
        self.assertEqual(self.topmap.fesa.class_name, expected['class_name'])
        self.assertEqual(self.topmap.fesa.class_version, expected['class_version'])
        self.assertEqual(self.topmap.fesa.creator, expected['creator'])
        self.assertEqual(self.topmap.fesa.framework_version, expected['framework_version'])
        self.assertEqual(self.topmap.fesa.description, expected['description'])

    def test_children(self):
        self.check_children(self.topmap, self.expected['children'])
        self.check_children(self.topmap, self.expected['children'], recursive=False, strict=True)

        self.check_children(self.afcz, self.expected_afcz['children'])
        self.check_children(self.afcz, self.expected_afcz['children'], recursive=False, strict=True)

if __name__ == '__main__':
    unittest.main()
