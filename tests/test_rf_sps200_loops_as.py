# PyCheby
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

import unittest
import itertools

import tests.config as config

from PyCheby.PyCheby import PyCheby
from tests.config import MEMMAP_PATH
from tests.testbase import RFTestCaseBase

class RFSPS200LoopsASTestCase(RFTestCaseBase):
    '''
    RF_SPS200_LOOPS Test Case for Address-Spaces

    The expected values are common to RF_SPS200_LOOPS and RF_SPS200_LOOPS_AS maps.
    The only difference is that the submaps shoudld be grouped into two Address-Spaces: bar0 and bar4.

    RF_SPS200_LOOPS maps will become deprecated and only maps in Address-Spaces format will be accepted.
    '''
    def setUp(self):
        self.expected = RFTestCaseBase.load_expected_results('RF_SPS200_LOOPS')
        self.expected_bar0 = self.expected['address-spaces']['bar0']
        self.expected_bar4 = self.expected['address-spaces']['bar4']

        self.memmap_path = MEMMAP_PATH['RF_SPS200_LOOPS_AS']
        self.tree = PyCheby(self.memmap_path, ignore_version_check=True)

        self.topmap = self.tree.get()
        self.bar0 = self.topmap.children['bar0']
        self.bar4 = self.topmap.children['bar4']

    def test_attrs(self):
        '''
        Check top memory map attributes
        '''
        self.assertEqual(self.tree.filename, self.expected['filename'])
        self.assertEqual(self.topmap.bus, self.expected['bus'])

    def test_attrs_size(self):
        '''
        Check size attributes
        '''
        self.assertEqual(self.topmap.c_size, self.expected['size'])
        self.assertEqual(self.bar0.c_size, self.expected_bar0['size'])
        self.assertEqual(self.bar4.c_size, self.expected_bar4['size'])

    def test_attrs_x_drv_edge(self):
        '''
        Check x_drv_edge attributes for the top map
        '''
        expected = self.expected['x-driver-edge']
        self.assertEqual(self.topmap.driver_edge.equipment_code, expected['equipment-code'])
        self.assertEqual(self.topmap.driver_edge.module_type, expected['module-type'])
        self.assertEqual(self.topmap.driver_edge.bus_type, expected['bus-type'])

    def test_children(self):
        self.check_children(self.bar0, self.expected_bar0['children'])
        self.check_children(self.bar0, self.expected_bar0['children'], recursive=False, strict=True)

        self.check_children(self.bar4, self.expected_bar4['children'])
        self.check_children(self.bar4, self.expected_bar4['children'], recursive=False, strict=True)

    def test_address_spaces(self):
        '''
        Address-Spaces miscelaneous tests
        '''
        def _err_msg(node, expected) -> str:
            return f'"{node.get_full_path()}": node address(={hex(node.address)}) != expected(={hex(expected)})'

        # check if Address-Spaces exist
        self.assertEqual(set(self.topmap.address_spaces), set([self.bar0, self.bar4]))

        # Extra check for BAR0 and BAR4 addresses
        self.assertEqual(self.bar0.address, self.expected_bar0['address'], msg=_err_msg(self.bar0, self.expected_bar0['address']))
        self.assertEqual(self.bar4.address, self.expected_bar4['address'], msg=_err_msg(self.bar4, self.expected_bar4['address']))

    def test_pci_bars(self):
        '''
        Check if the top map contains a PCI bar per each Address-Space
        '''
        # Check if PCI Bars match Address-Spaces
        expected_names = self.expected['pci-bars'].keys()
        self.assertTrue(self.bar0.name in expected_names)
        self.assertTrue(self.bar4.name in expected_names)

        # Check PCI BAR parameters
        expected = self.expected['pci-bars']
        pci_bars = [pci_bar for pci_bar in self.topmap.pci_bars if pci_bar.name in expected_names]
        for bar in pci_bars:
            self.assertEqual(bar.number,        expected[bar.name]['number'])
            self.assertEqual(bar.address_space, expected[bar.name]['address-space'])
            self.assertEqual(bar.base_addr,     expected[bar.name]['base-addr'])

if __name__ == '__main__':
    unittest.main()
