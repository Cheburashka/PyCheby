# PyCheby
# Copyright (C) 2023 CERN
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In applying this licence, CERN does not waive the privileges and immunities
# granted to it by virtue of its status as an Intergovernmental Organization or
# submit itself to any jurisdiction.

MEMMAP_DIR = 'tests/maps'

# Maps with '_AS' suffix are migrated to Address-Spaces
MEMMAP_PATH = {
    'RF_SPS_BEAM':          f'{MEMMAP_DIR}/RF_SPS_BEAM/AFCZ-BeamControl.cheby',
    'RF_SPS_BEAM_AS':       f'{MEMMAP_DIR}/RF_SPS_BEAM_AS/AFCZ-BeamControl.cheby',

    'RF_SPS200_LOOPS':      f'{MEMMAP_DIR}/RF_SPS200_LOOPS/sps200CavityControl.cheby',
    'RF_SPS200_LOOPS_AS':   f'{MEMMAP_DIR}/RF_SPS200_LOOPS_AS/sps200CavityControl.cheby',

    'BCTDC_DIDT_SPS':       f'{MEMMAP_DIR}/BCTDC_DIDT_SPS/BI_BCT24SPS.cheby',

    'RF_OPTCOMP':           f'{MEMMAP_DIR}/RF_OPTCOMP/optoLinkPhiComp.cheby',
    'RF_LN4_CAVLPV3':       f'{MEMMAP_DIR}/RF_LN4_CAVLPV3/Linac4CavityLoops.cheby',
}

RESULTS_FILE_PATH = 'tests/expected_results.yaml'
